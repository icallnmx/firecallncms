<?php
$webpage = 1;
require('global.php');
if(!isset($_SESSION['id'])) {
	header('Location: /index');
	exit();
}

if($session_infos->rank <= 6) {
	header('Location: /me');
	exit();
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?= $website_infos->nom; ?>: Administracion</title>
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/sty-le.css">
		<link rel="stylesheet" href="<?= $website_infos->lien; ?>/public/themify-icons/themify-icons.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php require_once('modeles/header.php'); ?>
			<div class="container-fluid content">
				<div class="container">
					<div class="col-md-8">
						<div class="module-index">
							<h1 style="margin-left: 10px;">Administracion</h1>
							<hr>
							<div class="content" style="padding: 0 10px 10px 10px;">
								Acceso solo para Administradores del hotel, aqui podras crear noticias, banear usuarios o desactivar cuentas, utilizalo con 
								sabiduria todo mal uso de estas herramientas sera notificado al dueño y el usuario sera sancionado.
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="module-black" style="height: auto;">
							<h1 style="margin: 0px;padding: 15px;">Navegacion</h1>
							<hr style="background-color: #232323;color:#232323;border-color: #323232;position: relative;margin: 0px;">
							<?php if($session_infos->rank >= 8) : ?>
							<a class="navset navaset" href="<?= $website_infos->lien; ?>/admin_article">Crear Noticia</a>
							<?php endif; ?>
							<?php if($session_infos->rank == 7 || $session_infos->rank >= 9) : ?>
							<a class="navset navaset" href="<?= $website_infos->lien; ?>/admin_ban">Banear usuario</a>
							<a class="navset navaset" href="<?= $website_infos->lien; ?>/admin_desac">Desactivar una cuenta</a>
							<?php endif; ?>
						</div>
					</div>

					<?php require_once('modeles/footer.php'); ?>

					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
					<script type="text/javascript">
					$(document).ready(function(){
						$("#loaderspin").css("display", "none");
					});
					</script>
				</div>
			</div>
		</div>
	</body>
</html>
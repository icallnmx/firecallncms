<?php
$webpage = 1;
require('global.php');
if(!isset($_SESSION['id'])) {
	header('Location: /index');
	exit();
}

if(isset($_GET['home']) AND !empty($_GET['home'])) {
	$home = htmlspecialchars($_GET['home']);
	$account = $bdd->prepare('SELECT * FROM users WHERE username = :username');
	$account->execute(['username' => $home]);
	if($account->rowCount() == 0) {
		$account = $bdd->prepare('SELECT * FROM users WHERE username = :username');
		$account->execute(['username' => $session_infos->username]);
	}
} else {
	$account = $bdd->prepare('SELECT * FROM users WHERE username = :username');
	$account->execute(['username' => $session_infos->username]);
}
$account_infos = $account->fetch();

// BADGES
$cb1 = $bdd->prepare('SELECT * FROM user_badges WHERE user_id = :user_id LIMIT 0,5');
$cb1->execute(['user_id' => $account_infos->id]);
$cb1_ = $cb1->rowCount();
$cb2 = $bdd->prepare('SELECT * FROM user_badges WHERE user_id = :user_id');
$cb2->execute(['user_id' => $account_infos->id]);
$cb2_ = $cb2->rowCount();

// GROUPES
$cg1 = $bdd->prepare('SELECT * FROM groups WHERE owner_id = :owner_id LIMIT 0,5');
$cg1->execute(['owner_id' => $account_infos->id]);
$cg1_ = $cg1->rowCount();
$cg2 = $bdd->prepare('SELECT * FROM groups WHERE owner_id = :owner_id');
$cg2->execute(['owner_id' => $account_infos->id]);
$cg2_ = $cg2->rowCount();

// AMIS
$ca1 = $bdd->prepare('SELECT * FROM messenger_friendships WHERE user_one_id = :user_one_id LIMIT 0,5');
$ca1->execute([
	'user_one_id' => $account_infos->id
]);
$ca1_ = $ca1->rowCount();
$ca2 = $bdd->prepare('SELECT * FROM messenger_friendships WHERE user_one_id = :user_one_id');
$ca2->execute([
	'user_one_id' => $account_infos->id
]);
$ca2_ = $ca2->rowCount();

// APPARTS
$cr1 = $bdd->prepare('SELECT * FROM rooms WHERE owner = :owner LIMIT 0,4');
$cr1->execute(['owner' => $account_infos->id]);
$cr1_ = $cr1->rowCount();
$cr2 = $bdd->prepare('SELECT * FROM rooms WHERE owner = :owner');
$cr2->execute(['owner' => $account_infos->id]);
$cr2_ = $cr2->rowCount();
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?= $website_infos->nom; ?>: Perfil</title>
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/sty-le.css">
		<link rel="stylesheet" href="<?= $website_infos->lien; ?>/public/themify-icons/themify-icons.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="Habbo" content="Habbo" />
		<meta name="twitter:card" content="summary"/>
		<meta name="twitter:site" content="@<?= $website_infos->twitter; ?>"/>
		<meta name="twitter:title" content="<?= $website_infos->nom; ?>: E&iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!."/>
		<meta name="twitter:description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!."/>
		<meta name="twitter:creator" content="@<?= $website_infos->twitter; ?>" />
		<meta name="twitter:image:src" content="https://i.imgur.com/jhQnyhw.png" />
		<meta name="twitter:domain" content="<?= $website_infos->lien; ?>"/>
		<meta name="identifier-url" content="<?= $website_infos->lien; ?>"/>
		<meta name="category" content="Rétro Habbo">
		<meta name="reply-to" content="<?= $website_infos->email; ?>">
		<meta property="og:site_name" content="<?= $website_infos->nom; ?> Hotel"/>
		<meta property="og:title" content="<?= $website_infos->nom; ?>: &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!."/>
		<meta property="og:url" content="<?= $website_infos->lien; ?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!."/>
		<meta property="og:image" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:image:secure_url" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:locale" content="es_ES"/>
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<meta name="Author" content="Cypher, Shone"/>
		<meta name="description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!."/>
		<meta name="keywords" content="habbox, habbo, virtuel, monde, réseau social, gratuit, communautée, avatar, chat, connectée, adolescence, jeu de rôle, rejoindre, social, groupes, forums, sécuritée, jouer, jeux, amis, rares, ados, jeunes, collector, collectionner, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, badges, musique, chat vip, fun, sortir, mmo, mmorpg, jeu massivement multijoueur, habbo, habboworld, habbodreams, jabbo, habbo hotel, habbo gratuit, habbo credit, habbocity, habbo-city, hbc, hcity, habbo city, bobba, bobbah hotel, bobbahotel, bobba hotel, bobba-hotel, jabbo, jabbo hotel, jabbonow, jabbohotel, jabborp, habbolove, habbo-love, habbo love, hlove, habbolove inscription, habbo, HABBO, habboo, retro habbo, rétro habbo, serveur habbo, retro, habbo retro gratuit, autre habbo, habbo autre, habbo retro qui marche bien, jeu comme habbo, jeux comme habbo, site comme habbo, habbo site, serveur privé habbo, habbo beta, hbeta, habbobeta, habbo-beta, habbo-dreams, habbo dreams, habbo dream, habbo-dreams, cola-hotel, cola hotel, bobbaworld, bobba-world, world, worldhabbo, world-habbo, habbiworld, habbo world, hworld, zunny, abbo, habbi, abboz, habboz, habbo gratuit, adohotel, adoh, ado-h, habbo credit, habbo hotel, habbo hotel gratuit, jouer a habbo gratuitement, habbo en gratuit, habbo retro, recrutement staff, recrutement, mmorpg, vip, animateur, animation, jeu du celib, clack ou smack, staff, rencontre, celibataire, casino, rares, magots, enable, boutique, fifa, foot, cheval, chevaux, piscine, crédits gratuits, crédit gratuit, staff club, virtuel, monde, réseau social, gratuit, communauté, avatar, chat, connecté, adolescence, jeu de rôle, rejoindre, social, groupes, forums, jouer, jeux, amis, ados, jeunes, collector, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, création, badges, musique, célébrité, chat vip, fun, sortir, mmo, chat, youtube, facebook, twitter"/>
	</head>
	<body>
		<style>
		li.item--badge2 {
		    float: left;
		    width: calc(100% / 5) !important;
		}
		.usercontent {
			padding: 8px;
		    display: table;
			width: 100%;
		}
		</style>
		<?php require_once('modeles/header.php'); ?>
			<div class="col-md-12" style="padding:0px"> 
				<div class="module-index" style="background: #e9e9e9;">
					<div class="content" style="text-align:  center;padding: 0px;display: table;">
						<div class="container-fluid content">
							<div class="container">
								<div class="col-md-6">
									<div class="usercontent">
										<div style="display: inline-table;float: left;background: #3a3a3a;width: 120px;height: 120px;border-radius:  50%;text-align:  center;border: solid 2px grey;position: relative;z-index:  2;">
											<img alt="" src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $account_infos->look; ?>&direction=3&head_direction=3&gesture=sml&action=&size=m" style="display: inline-block;">
										</div>
										<div style="display: flex;float: left;margin-left: -15px;height: 120px;vertical-align: baseline;justify-content: center;">
											<div style="margin: auto auto;background: #3a3a3a;padding: 5px 30px;width: 300px;border: solid 2px grey;border-radius: 5px;color: white;">
												<div style="padding:  0px;font-size:  18px;font-weight: bold;display: block;text-align: left;"> <?= $account_infos->username; ?>
													<div style="display: inline-block;width: 10px;height: 10px;background: <?php if($account_infos->online == 1) echo 'green'; else echo 'red'; ?>;border-radius: 50px;"></div>
												</div>
												<div style="padding:  0px;font-size:  15px;display: block;text-align: left;"> <i><?= htmlspecialchars(utf8_encode($account_infos->motto)); ?></i> </div>
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6" style="height: 135px;vertical-align: baseline;justify-content: center;display: flex;">
									<form method="post" style="display: inline-block;width: 100%;margin: auto auto;">
										<input type="text" id="pseudo" name="pseudo" placeholder="Pseudo" style="width: 70%;float: left;margin:  auto;height: 50px;border-radius: 4px 0px 0px 4px;background: white;">
										<button id="submitsearchpseudo" name="submitsearchpseudo" style="width: 30%;position: static;margin: auto;border-radius: 0px 4px 4px 0px;">Buscar</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>

			<div class="container-fluid content">
				<div class="container">
					<div class="col-md-12" style="padding:0px">
						<div class="col-md-6"> 
							<div class="module-index">
								<center>
									<h1> Placas <span class="profile__friends__count">(<?= $cb1_; ?> sur <?= $cb2_; ?>)</h1>
								</center><hr>
								<div class="content" style="text-align:  center;">
									<?php while($cb1_infos = $cb1->fetch()) : ?>
									<li class="item--badge" style="width: 17.5%;display: inline-block;height: 120px;padding: 5px;">
										<img alt="" src="<?= $website_infos->album1584; ?><?= $cb1_infos->badge_id; ?>.gif" style="margin-top: 20px;">
										<div class="item__text">
											
										</div>
									</li>
									<?php endwhile; ?>
									<button class="form__submit2" onclick="displayBadge()"> Ver mas &raquo; </button>
								</div>
							</div>
						</div>

						<div class="col-md-6"> 
							<div class="module-index">
								<center>
									<h1> Grupos <span class="profile__friends__count">(<?= $cg1_; ?> sur <?= $cg2_; ?>)</h1>
								</center><hr>
								<div class="content" style="text-align:  center;">
									<?php while($cg1_infos = $cg1->fetch()) : ?>
									<li class="item--badge2" style="height: 120px;padding: 5px;display: block;">
										<img alt="" src="https://www.habbox.fr/badge/<?= $cg1_infos->badge; ?>.gif" style="margin-top: 20px;">
										<div class="item__text">
											<h5 style="margin: 10px 0px 0px 0px;font-weight: bold;"><?= htmlspecialchars(utf8_encode($cg1_infos->name)); ?></h5>
										</div>
									</li>
									<?php endwhile; ?>
									<button class="form__submit2" onclick="displaygroupes()"> Ver mas &raquo; </button>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12" style="padding:0px">
						<div class="col-md-6"> 
							<div class="module-index">
								<center>
									<h1> Amigos <span class="profile__friends__count">(<?= $ca1_; ?> sur <?= $ca2_; ?>)</h1>
								</center>
								<hr>
								<div class="content" style="text-align:  center;">
									<?php
									while($ca1_infos = $ca1->fetch()) :
									$ac = $bdd->prepare('SELECT look,username,motto FROM users WHERE id = :id');
									$ac->execute(['id' => $ca1_infos->user_two_id]);
									$ac_infos = $ac->fetch();
									?>

									<li class="item--badge" style="width: 17.5%;display: inline-block;margin-bottom: 10px;padding: 5px;height: 150px;">
										<img alt="" src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $ac_infos->look; ?>&direction=3&head_direction=3&gesture=sml&action=&size=m">
										<div class="item__text">
											<h5 style="margin: 10px 0px 0px 0px;font-weight: bold;"><?= $ac_infos->username; ?></h5>
										</div>
									</li>
									<?php endwhile; ?>
									<button class="form__submit2" onclick="displayfriends()"> Ver mas &raquo; </button>
								</div>
							</div>
						</div>
						<div class="col-md-6"> 
							<div class="module-index">
								<center>
									<h1> Salas <span class="profile__friends__count">(<?= $cr1_; ?> sur <?= $cr2_; ?>)</h1>
								</center><hr>
								<div class="content" style="text-align:  center;">
									<?php while($cr1_infos = $cr1->fetch()) : ?>
									<div class="col-md-3">
										<li class="item--badge" style="display: inline-block;margin-bottom: 10px;padding: 5px;height: 150px;">
											<div style="width: 110px;height: 110px;margin-bottom: 5px;">
												<img alt="" src="<?= $website_infos->lien; ?>/public/images/1511760814.png" style="border-radius: 5px;">
											</div>
											<div class="item__text">
												<h5 style="margin: 10px 0px 0px 0px;font-weight: bold;"><?= htmlspecialchars(utf8_encode($cr1_infos->caption)); ?></h5>
											</div>
										</li>
									</div>
									<?php endwhile; ?>
									<button class="form__submit2" onclick="displayapparts()"> Ver mas &raquo; </button>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12" style="text-align: center;display: block;">
						<center>
						</center>
					</div>


					<?php require_once('modeles/footer.php'); ?>

					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
					<script type="text/javascript">
					$(document).ready(function(){
						$("#submitsearchpseudo").on('click', function(event){
							event.preventDefault();
							$.ajax({
								type: "POST",
								url: "<?= $website_infos->lien; ?>/req/searchpseudo.php",
								data: "pseudo="+$("#pseudo").val(),
								success: function(msg){
									if(msg == "ok") {
										window.location = "<?= $website_infos->lien; ?>/home/"+document.getElementById("pseudo").value;
									}else{
										swal("Oops", msg,  "error");
									}
								}
							});
						});
						$("#loaderspin").css("display", "none");
					});
					</script>
				</div>
			</div>
			<div id="Modalb" class="modal" style="background: rgba(0, 0, 0, 0.61);overflow: auto;">
				<div style="display: block;width: 50%;max-width: 550px;min-width: 300px;margin: 50px auto;overflow: auto;">
					<div class="module-index" style="height: auto;">
						<h1 style="margin-right: 20px;">
							<center>Placas<span class="close" onclick="displayBadgeclose()">&times;</span></center>
						</h1>
						<hr>
						<div class="content" style="text-align:  center;">
  							<?php while($cb2_infos = $cb2->fetch()) : ?>
  							<ul id="badge_ul" style="padding: 0px;">
								<li class="item--badgebig" style="width: 100%;padding: 5px;display: table;line-height: 50px;white-space: nowrap;">
									<img alt="" src="<?= $website_infos->album1584; ?><?= $cb2_infos->badge_id; ?>.gif" style="float: left;">
									<div class="item__text">
										<h5 style="margin:0px;"><?= $cb2_infos->badge_id; ?></h5>
									</div>
								</li>
							</ul>
							<?php endwhile; ?>
						</div>
					</div>
				</div>
			</div>

			<div id="Modalf" class="modal" style="background: rgba(0, 0, 0, 0.61);overflow: auto;">
				<div style="display: block;width: 50%;max-width: 550px;min-width: 300px;margin: 50px auto;overflow: auto;">
					<div class="module-index" style="height: auto;">
						<h1 style="margin-right: 20px;">
							<center>Amigos<span class="close" onclick="displayfriendsclose()">&times;</span></center>
						</h1>
						<hr>
						<div class="content" style="text-align:  center;">
							<?php
							while($ca2_infos = $ca2->fetch()) :
							$ac = $bdd->prepare('SELECT look,username,motto FROM users WHERE id = :id');
							$ac->execute(['id' => $ca2_infos->user_two_id]);
							$ac_infos = $ac->fetch();
							?>
							<ul id="friends_ul" style="padding: 0px;">
								<li class="item--badgebig" style="width: 100%;padding: 5px;display: table;">
									<img alt="" src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $ac_infos->look; ?>&direction=3&head_direction=3&gesture=sml&action=&size=m" style="float: left;">
									<div class="item__text" style="height: 67px;align-items: center;display: inline-grid;margin-top: 20px;">
										<h5 style="margin:0px;"><?= $ac_infos->username; ?></h5>
										<h6 style="margin:0px;"><?= htmlspecialchars(utf8_encode($ac_infos->motto)); ?></h6>
									</div>
								</li>
							</ul>
							<?php endwhile; ?>
  						</div>
					</div>
				</div>
			</div>

			<div id="Modalap" class="modal" style="background: rgba(0, 0, 0, 0.61);overflow: auto;">
				<div style="display: block;width: 50%;max-width: 550px;min-width: 300px;margin: 50px auto;overflow: auto;">
					<div class="module-index" style="height: auto;">
						<h1 style="margin-right: 20px;">
							<center>Salas<span class="close" onclick="displayappartsclose()">&times;</span></center>
						</h1>
						<hr>
						<div class="content" style="text-align:  center;">
							<?php while($cr2_infos = $cr2->fetch()) : ?>
							<ul id="appart_ul" style="padding: 0px;">
								<li class="item--badgebig" style="width: 100%;padding: 5px;display: table;line-height: 110px;white-space: nowrap;">
									<div style="width: 110px;height: 110px;margin-bottom: 5px;display: inline-block;float: left;">
										<img alt="" src="<?= $website_infos->lien; ?>/public/images/1511760814.png" style="border-radius: 5px;">
									</div>

									<div class="item__text">
										<h5 style="margin:0px;"><?= htmlspecialchars(utf8_encode($cr2_infos->caption)); ?></h5>
									</div>
								</li>
							</ul>
							<?php endwhile; ?>
			  			</div>
					</div>
				</div>
			</div>

			<div id="Modalgr" class="modal" style="background: rgba(0, 0, 0, 0.61);overflow: auto;">
				<div style="display: block;width: 50%;max-width: 550px;min-width: 300px;margin: 50px auto;overflow: auto;">
					<div class="module-index" style="height: auto;">
						<h1 style="margin-right: 20px;">
							<center>Grupos<span class="close" onclick="displaygroupesclose()">&times;</span></center>
						</h1>
						<hr>
						<div class="content" style="text-align:  center;">
							<?php while($cg2_infos = $cg2->fetch()) : ?>
							<ul id="groups_ul" style="padding: 0px;">
								<li class="item--badgebig" style="width: 100%;padding: 5px;display: table;line-height: 110px;white-space: nowrap;">
									<img alt="" src="https://www.habbox.fr/badge/<?= $cg2_infos->badge; ?>.gif" style="float:left;">
									<div class="item__text">
										<h5 style="margin:0px;"><?= htmlspecialchars(utf8_encode($cg2_infos->name)); ?></h5>
									</div>
								</li>
							</ul>
							<?php endwhile; ?>
			  			</div>
					</div>
				</div>
			</div>
			<script>
			function displayBadge() {
			        document.getElementById("Modalb").style.display = "block";
					document.getElementsByTagName('html')[0].style.overflow = "hidden";
			}
			function displayBadgeclose() {
			        document.getElementById("Modalb").style.display = "none";
					document.getElementsByTagName('html')[0].style.overflow = "auto";
			}
			function displayfriends() {
			        document.getElementById("Modalf").style.display = "block";
					document.getElementsByTagName('html')[0].style.overflow = "hidden";
			}
			function displayBadge() {
			    document.getElementById("Modalb").style.display = "block";
			    document.getElementsByTagName('html')[0].style.overflow = "hidden";
			}

			function displayBadgeclose() {
			    document.getElementById("Modalb").style.display = "none";
			    document.getElementsByTagName('html')[0].style.overflow = "auto";
			}

			function displayfriends() {
			    document.getElementById("Modalf").style.display = "block";
			    document.getElementsByTagName('html')[0].style.overflow = "hidden";
			}

			function displayfriendsclose() {
			    document.getElementById("Modalf").style.display = "none";
			    document.getElementsByTagName('html')[0].style.overflow = "auto";
			}

			function displayapparts() {
			    document.getElementById("Modalap").style.display = "block";
			    document.getElementsByTagName('html')[0].style.overflow = "hidden";
			}

			function displayappartsclose() {
			    document.getElementById("Modalap").style.display = "none";
			    document.getElementsByTagName('html')[0].style.overflow = "auto";
			}

			function displaygroupes() {
			    document.getElementById("Modalgr").style.display = "block";
			    document.getElementsByTagName('html')[0].style.overflow = "hidden";
			}

			function displaygroupesclose() {
			    document.getElementById("Modalgr").style.display = "none";
			    document.getElementsByTagName('html')[0].style.overflow = "auto";
			}
			window.onclick = function(event) {
			    if (event.target == document.getElementById("Modalf")) {
			        document.getElementById("Modalf").style.display = "none";
			        document.getElementsByTagName('html')[0].style.overflow = "auto";
			    }
			    if (event.target == document.getElementById("Modalb")) {
			        document.getElementById("Modalb").style.display = "none";
			        document.getElementsByTagName('html')[0].style.overflow = "auto";
			    }
			    if (event.target == document.getElementById("Modalap")) {
			        document.getElementById("Modalap").style.display = "none";
			        document.getElementsByTagName('html')[0].style.overflow = "auto";
			    }
			    if (event.target == document.getElementById("Modalgr")) {
			        document.getElementById("Modalgr").style.display = "none";
			        document.getElementsByTagName('html')[0].style.overflow = "auto";
			    }
			}

			var slideIndex = 1;
			showSlides(slideIndex);

			function plusSlides(n) {
			    showSlides(slideIndex += n);
			}

			function currentSlide(n) {
			    showSlides(slideIndex = n);
			}

			function showSlides(n) {
			    var i;
			    var slides = document.getElementsByClassName("mySlides");
			    var dots = document.getElementsByClassName("dot");
			    if (n > slides.length) {
			        slideIndex = 1
			    }
			    if (n < 1) {
			        slideIndex = slides.length
			    }
			    for (i = 0; i < slides.length; i++) {
			        slides[i].style.display = "none";
			    }
			    for (i = 0; i < dots.length; i++) {
			        dots[i].className = dots[i].className.replace(" active", "");
			    }
			    slides[slideIndex - 1].style.display = "block";
			    dots[slideIndex - 1].className += " active";
			}
			function displayfriendsclose() {
			        document.getElementById("Modalf").style.display = "none";
					document.getElementsByTagName('html')[0].style.overflow = "auto";
			}
			function displayapparts() {
			        document.getElementById("Modalap").style.display = "block";
					document.getElementsByTagName('html')[0].style.overflow = "hidden";
			}
			function displayappartsclose() {
			        document.getElementById("Modalap").style.display = "none";
					document.getElementsByTagName('html')[0].style.overflow = "auto";
			}
			function displaygroupes() {
			        document.getElementById("Modalgr").style.display = "block";
					document.getElementsByTagName('html')[0].style.overflow = "hidden";
			}
			function displaygroupesclose() {
			        document.getElementById("Modalgr").style.display = "none";
					document.getElementsByTagName('html')[0].style.overflow = "auto";
			}
			window.onclick = function(event) {
			    if (event.target == document.getElementById("Modalf")) {
			        document.getElementById("Modalf").style.display = "none";
					document.getElementsByTagName('html')[0].style.overflow = "auto";
			    } 
				if (event.target == document.getElementById("Modalb")) {
			        document.getElementById("Modalb").style.display = "none";
					document.getElementsByTagName('html')[0].style.overflow = "auto";
			    }
				if (event.target == document.getElementById("Modalap")) {
			        document.getElementById("Modalap").style.display = "none";
					document.getElementsByTagName('html')[0].style.overflow = "auto";
			    }
				if (event.target == document.getElementById("Modalgr")) {
			        document.getElementById("Modalgr").style.display = "none";
					document.getElementsByTagName('html')[0].style.overflow = "auto";
			    }
			}

			var slideIndex = 1;
			showSlides(slideIndex);

			function plusSlides(n) {
			  showSlides(slideIndex += n);
			}

			function currentSlide(n) {
			  showSlides(slideIndex = n);
			}

			function showSlides(n) {
			  var i;
			  var slides = document.getElementsByClassName("mySlides");
			  var dots = document.getElementsByClassName("dot");
			  if (n > slides.length) {slideIndex = 1}    
			  if (n < 1) {slideIndex = slides.length}
			  for (i = 0; i < slides.length; i++) {
			      slides[i].style.display = "none";  
			  }
			  for (i = 0; i < dots.length; i++) {
			      dots[i].className = dots[i].className.replace(" active", "");
			  }
			  slides[slideIndex-1].style.display = "block";  
			  dots[slideIndex-1].className += " active";
			}
			</script>
		</div>
	</body>
</html>
<?php

namespace JBBCode;

require_once 'CodeDefinition.php';
require_once 'CodeDefinitionBuilder.php';
require_once 'CodeDefinitionSet.php';
require_once 'validators/CssColorValidator.php';
require_once 'validators/UrlValidator.php';

/**
 * Provides a default set of common bbcode definitions.
 *
 * @author jbowens
 */
class DefaultCodeDefinitionSet implements CodeDefinitionSet
{

    /* The default code definitions in this set. */
    protected $definitions = array();

    /**
     * Constructs the default code definitions.
     */
    public function __construct()
    {
        /* [b] bold tag */
        $builder = new CodeDefinitionBuilder('b', '<strong>{param}</strong>');
        array_push($this->definitions, $builder->build());

        /* [i] italics tag */
        $builder = new CodeDefinitionBuilder('i', '<em>{param}</em>');
        array_push($this->definitions, $builder->build());

        /* [u] underline tag */
        $builder = new CodeDefinitionBuilder('u', '<u>{param}</u>');
        array_push($this->definitions, $builder->build());

        $urlValidator = new \JBBCode\validators\UrlValidator();

        /* [url] link tag */
        $builder = new CodeDefinitionBuilder('url', '<a href="{param}">{param}</a>');
        $builder->setParseContent(false)->setBodyValidator($urlValidator);
        array_push($this->definitions, $builder->build());

        /* [url=http://example.com] link tag */
        $builder = new CodeDefinitionBuilder('url', '<a href="{option}">{param}</a>');
        $builder->setUseOption(true)->setParseContent(true)->setOptionValidator($urlValidator);
        array_push($this->definitions, $builder->build());

        /* [img] image tag */
        $builder = new CodeDefinitionBuilder('img', '<img src="{param}" />');
        $builder->setUseOption(false)->setParseContent(false)->setBodyValidator($urlValidator);
        array_push($this->definitions, $builder->build());

        /* [img=alt text] image tag */
        $builder = new CodeDefinitionBuilder('img', '<img src="{param}" alt="{option}" />');
        $builder->setUseOption(true)->setParseContent(false)->setBodyValidator($urlValidator);
        array_push($this->definitions, $builder->build());

        /* [color] color tag */
        $builder = new CodeDefinitionBuilder('color', '<span style="color: {option}">{param}</span>');
        $builder->setUseOption(true)->setOptionValidator(new \JBBCode\validators\CssColorValidator());
        array_push($this->definitions, $builder->build());

        $builder = new CodeDefinitionBuilder('list', '<ul>{param}</ul>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('list', '<ol>{param}</ol>');
        $builder->setUseOption(true);
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('size', '<font size="{option}">{param}</font>');
        $builder->setUseOption(true)->setParseContent(true);
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('font', '<span style="font-family: {option}">{param}</span>');
        $builder->setUseOption(true);
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('left', '<div style="text-align:left;">{param}</div>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('right', '<div style="text-align:right;">{param}</div>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('center', '<div style="text-align:center;">{param}</div>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('code', '<pre>{param}</pre>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('table', '<table class="table forum">{param}</table>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('td', '<td>{param}</td>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('tr', '<tr>{param}</tr>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('quote', '<blockquote>{param}</blockquote>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('s', '<s>{param}</s>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('sup', '<sup>{param}</sup>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('sub', '<sub>{param}</sub>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('p', '<p>{param}</p>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('ol', '<ol>{param}</ol>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('ul', '<ul>{param}</ul>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('li', '<li>{param}</li>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('video', '<iframe width="560" height="315" src="http://www.youtube.com/embed/{param}" frameborder="0"></iframe><br/><br/>');
        array_push($this->definitions, $builder->build());
        $builder = new CodeDefinitionBuilder('*', '<li>{param}</li>');
        array_push($this->definitions, $builder->build());
    }

    /**
     * Returns an array of the default code definitions.
     */
    public function getCodeDefinitions() 
    {
        return $this->definitions;
    }

}

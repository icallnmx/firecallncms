<?php
$webpage = 2;
require('global.php');

if(!isset($_SESSION['id'])) {
	header('Location: /index');
	exit();
}

require('JBBCode/Parser.php');
$parser = new JBBCode\Parser();
$parser->addCodeDefinitionSet(new JBBCode\DefaultCodeDefinitionSet());

if(isset($_GET['id']) AND !empty($_GET['id'])) {
	$id = intval($_GET['id']);
	$article = $bdd->prepare('SELECT * FROM habboxcms_articles WHERE id = :id');
	$article->execute(['id' => $id]);
	if($article->rowCount() == 1) {
	} else {
		$article = $bdd->query('SELECT * FROM habboxcms_articles ORDER BY id DESC LIMIT 0,1');
	}
} else {
	$article = $bdd->query('SELECT * FROM habboxcms_articles ORDER BY id DESC LIMIT 0,1');
}
$article_infos = $article->fetch();
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?= $website_infos->nom; ?>: <?= $article_infos->titre; ?></title>
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/sty-le.css">
		<link rel="stylesheet" href="<?= $website_infos->lien; ?>/public/themify-icons/themify-icons.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="Habbo" content="Habbo" />
		<meta name="twitter:card" content="summary"/>
		<meta name="twitter:site" content="@<?= $website_infos->twitter; ?>"/>
		<meta name="twitter:title" content="<?= $website_infos->nom; ?>: &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="twitter:description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="twitter:creator" content="@<?= $website_infos->twitter; ?>" />
		<meta name="twitter:image:src" content="https://i.imgur.com/jhQnyhw.png" />
		<meta name="twitter:domain" content="<?= $website_infos->lien; ?>"/>
		<meta name="identifier-url" content="<?= $website_infos->lien; ?>"/>
		<meta name="category" content="Rétro Habbo">
		<meta name="reply-to" content="<?= $website_infos->email; ?>">
		<meta property="og:site_name" content="<?= $website_infos->nom; ?> Hotel"/>
		<meta property="og:title" content="<?= $website_infos->nom; ?>: &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta property="og:url" content="<?= $website_infos->lien; ?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta property="og:image" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:image:secure_url" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:locale" content="es_ES"/>
		<meta name="Author" content="Cypher, Shone"/>
		<meta name="description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="keywords" content="habbox, habbo, virtuel, monde, réseau social, gratuit, communautée, avatar, chat, connectée, adolescence, jeu de rôle, rejoindre, social, groupes, forums, sécuritée, jouer, jeux, amis, rares, ados, jeunes, collector, collectionner, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, badges, musique, chat vip, fun, sortir, mmo, mmorpg, jeu massivement multijoueur, habbo, habboworld, habbodreams, jabbo, habbo hotel, habbo gratuit, habbo credit, habbocity, habbo-city, hbc, hcity, habbo city, bobba, bobbah hotel, bobbahotel, bobba hotel, bobba-hotel, jabbo, jabbo hotel, jabbonow, jabbohotel, jabborp, habbolove, habbo-love, habbo love, hlove, habbolove inscription, habbo, HABBO, habboo, retro habbo, rétro habbo, serveur habbo, retro, habbo retro gratuit, autre habbo, habbo autre, habbo retro qui marche bien, jeu comme habbo, jeux comme habbo, site comme habbo, habbo site, serveur privé habbo, habbo beta, hbeta, habbobeta, habbo-beta, habbo-dreams, habbo dreams, habbo dream, habbo-dreams, cola-hotel, cola hotel, bobbaworld, bobba-world, world, worldhabbo, world-habbo, habbiworld, habbo world, hworld, zunny, abbo, habbi, abboz, habboz, habbo gratuit, adohotel, adoh, ado-h, habbo credit, habbo hotel, habbo hotel gratuit, jouer a habbo gratuitement, habbo en gratuit, habbo retro, recrutement staff, recrutement, mmorpg, vip, animateur, animation, jeu du celib, clack ou smack, staff, rencontre, celibataire, casino, rares, magots, enable, boutique, fifa, foot, cheval, chevaux, piscine, crédits gratuits, crédit gratuit, staff club, virtuel, monde, réseau social, gratuit, communauté, avatar, chat, connecté, adolescence, jeu de rôle, rejoindre, social, groupes, forums, jouer, jeux, amis, ados, jeunes, collector, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, création, badges, musique, célébrité, chat vip, fun, sortir, mmo, chat, youtube, facebook, twitter"/>
	</head>
	<body>
		<style>
		li {
		    list-style: none;
		}
		</style>
		<?php require_once('modeles/header.php'); ?>
			<div class="container-fluid content">
				<div class="container">
					<div class="col-md-8">
						<div class="module-index">
							<h1 style="width: auto;word-break: break-all;"><?= $article_infos->titre; ?></h1>
							<hr>
							<div class="content" style="">
								<?php $parser->parse($article_infos->contenu); echo nl2br($parser->getAsHtml()); ?>
							</div>
						</div>

						<div class="module-index">
							<h1>Comentarios</h1>
							<hr>
							<div class="content newscom" style="">
								<?php
								$les_commentaires = $bdd->prepare('SELECT * FROM habboxcms_a_comments WHERE id_article = :id_article ORDER BY id DESC');
								$les_commentaires->execute(['id_article' => $article_infos->id]);
								while($les_commentaires_infos = $les_commentaires->fetch()) {
								$account = $bdd->prepare('SELECT username,look,rank FROM users WHERE id = :id');
								$account->execute(['id' => $les_commentaires_infos->user_id]);
								if($account->rowCount() == 1) {
									$account_infos = $account->fetch();
								}
								?>
								<table class="ranking-user" style="padding:5px;width:100%;">
									<tbody>
										<tr>
											<td valign="middle" width="15">
												<a href="<?= $website_infos->lien; ?>/home/<?= $account_infos->username; ?>">
													<div style="filter: drop-shadow(3px 2px 0 #fff) drop-shadow(-3px 1px 0 #fff) drop-shadow(0 -3px 0 #fff);width: 60px;height:120px;margin-top: -15px;margin-left: 0px;float: right;background: url(https://habbo.com/habbo-imaging/avatarimage?figure=<?= $account_infos->look; ?>&direction=2&head_direction=3&gesture=sml&size=m);"></div>
												</a>
											</td>
											<td valign="top">
												<div style="margin-left:15px;margin-top:10px;">
													<a href="<?= $website_infos->lien; ?>/home/<?= $account_infos->username; ?>" style="color: black;"><b><?= $account_infos->username; ?></b></a></br>
													<r style="width:auto;word-break: break-all;"><?= $les_commentaires_infos->commentaire; ?></r><?php if($account_infos->rank >= 7) : ?><img src="<?= $website_infos->album1584; ?>ADM.gif" style="filter: none;margin-top: -25px;margin-left: 0px;float: right;"><?php endif; ?>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
								<i style="display: block;float: right;margin-top: -24px;">Posteado en: <?= date('d/m/Y H:i', $les_commentaires_infos->date_post); ?></i>
								<hr style="margin-bottom: 20px;border-top-width: 0px;margin:0px;border-width: thin;border-color: #0e3955;border: solid 0.5px #0e3955;display: none;">
								<?php } ?>
							</div>

							<form style="margin-top: 15px;" method="post">
								<label><b> Escribir un comentario</b></label>
								<div style="filter: drop-shadow(3px 2px 0 #fff) drop-shadow(-3px 1px 0 #fff) drop-shadow(0 -3px 0 #fff);width: 60px;height:120px;margin-top: -15px;margin-left: 0px;float: left;background: url(https://habbo.com/habbo-imaging/avatarimage?figure=<?= $session_infos->look; ?>&direction=2&head_direction=3&gesture=sml&size=m);margin-right: 10px;"></div>
								<textarea style="width: 89%;height: 100px;color: black;" name="com" id="com" value=""></textarea>
								<div class="form__submit2" style="margin-top: 10px;" onclick="com('<?= $article_infos->id; ?>')">Escribir</div>
							</form>
						</div>
					</div>

					<div class="col-md-4"> 
						<div class="module-black" style="height: auto;">
							<?php
							$les_articles = $bdd->query('SELECT id,titre FROM habboxcms_articles ORDER BY id DESC LIMIT 0,15');
							while($les_articles_infos = $les_articles->fetch()) {
							?>
							<a href="<?= $website_infos->lien; ?>/articles/<?= $les_articles_infos->id; ?>" class="navset navaset" style="width: auto;word-break: break-all;"><?= $les_articles_infos->titre; ?></a>
							<?php } ?>
						</div>
					</div>

					<?php require_once('modeles/footer.php'); ?>
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
					<script type="text/javascript">
					$(document).ready(function(){
						$("#loaderspin").css("display", "none");
					});
					function com(id){
						var com = $("#com").val();
    					$.ajax({
        					url: "<?= $website_infos->lien; ?>/req/com.php",
				            type: "POST",
				            data: {'id' : id, 'com' : com},                
				            success: function(data){
            					if(data == 'Success'){
									$(".newscom").load("<?= $website_infos->lien; ?>/articles/"+id+" .newscom");
										swal("Bien!", "Usted ha enviado su comentario.",  "success");
             					}else{
                					swal("Oups", data, "error");
            					}                             
        					}
    					});
					}
					</script>
				</div>
			</div>
		</div>
	</body>
</html>
<?php if(isset($website_infos)) : ?>
<div id="siteapp" style="width: 100%;">
	<div class="container-fluid">
		<header>
			<div class="container-fluid" style="height: 100%;">	
				<div class="container" style="height: 100%;">
					<div class="logocontent" style="display: inline-flex;vertical-align: baseline;justify-content: center;height: 100%;">
						<div class="logo" style="background: url(https://hsource.fr/font/habbo_new/<?= $website_infos->nom; ?>.gif) no-repeat;height:80px;width:250px;"></div>
					</div>
					<?php if(isset($_SESSION['id'])) : ?>
					<div style="display: inline-flex;vertical-align: baseline;justify-content: center;height: 100%;float: right;">
						<div class="avatar_header">
							<div class="avatar_header--pseudo"><?= $session_infos->username; ?></div>
							<div class="avatar_header--avatar">
								<img src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $session_infos->look; ?>&direction=3&head_direction=3&gesture=sml&action=&size=m&headonly=1">
							</div>
							<div class="avatar_header--search">
								<a href="<?= $website_infos->lien; ?>/home" class="btnaheader prof">Mi perfil</a>
								<a href="<?= $website_infos->lien; ?>/settings" class="btnaheader para">Configuracion</a>
								<a href="<?= $website_infos->lien; ?>/account/logout" class="btnaheader deco">Desconectar</a>
							</div>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</header>
	</div>

	<div class="container-fluid">	
		<nav>
			<div class="container nav">
				<div class="col-md-12">
					<a href="<?= $website_infos->lien; ?>/index">
						<span class="glyphicon glyphicon-home" style="position: relative; top: 3px;display: inline-block;"></span>
						<div class="nameh">Bienvenid@</div>
					</a>
					<a href="<?= $website_infos->lien; ?>/community/news">
						<span class="glyphicon glyphicon-globe" style="position: relative; top: 3px;display: inline-block;"></span>
						<div class="nameh">Comunidad</div>
					</a>
					<?php if(isset($_SESSION['id'])) : ?>
					<a href="<?= $website_infos->lien; ?>/shop/diamants">
						<span class="glyphicon glyphicon-shopping-cart" style="position: relative; top: 3px;display: inline-block;"></span>
						<div class="nameh">Tienda</div>
					</a>
					<?php endif; ?>
					<a href="<?= $website_infos->lien; ?>/recrutements">
						<span class="glyphicon glyphicon-envelope" style="position: relative; top: 3px;display: inline-block;"></span>
						<div class="nameh">Reclutamiento</div>
					</a>
					<?php if(isset($_SESSION['id'])) : ?>
					<div class="container-button">
						<a class="button_hotel" onclick="window.open('<?= $website_infos->lien; ?>/hotel'); preventDefault();">
							<span class="glyphicon glyphicon-globe" aria-hidden=true></span> Hotel
						</a>
					</div>
					<?php if($session_infos->rank >= 7) : ?>
					<div class="container-button" style="margin-right:10px;">
						<a class="button_hotel" onclick="window.open('<?= $website_infos->lien; ?>/admin_index.php'); preventDefault();">
							<span class="glyphicon glyphicon-hand-right" aria-hidden=true></span> Administracion
						</a>
					</div>
					<?php endif; endif; ?>
				</div>
			</div>
		</nav>
	</div>

	<div class="container-fluid">
		<nav class="submenu">
			<div class="container sub">
				<div class="col-md-12">
					<?php if(isset($webpage, $_SESSION['id'])) : ?>
					<?php if($webpage == 1) : ?>
					<a href="<?= $website_infos->lien; ?>/me"><?= $session_infos->username; ?></a>
					<a href="<?= $website_infos->lien; ?>/settings">Configuracion</a>
					<a href="<?= $website_infos->lien; ?>/compte">Estadisticas</a>
					<!-----<a onclick="window.open('https://trouveretro.fr/vote/habbox'); preventDefault();" style="text-transform: uppercase">Vota por <?= $website_infos->nom; ?></a>---->
					<?php elseif($webpage == 2) : ?>
					<a href="<?= $website_infos->lien; ?>/community/news">Notícias</a>
					<a href="<?= $website_infos->lien; ?>/community/photos">Fotos</a>
					<a href="<?= $website_infos->lien; ?>/community/apparts">Salas</a>
					<a href="<?= $website_infos->lien; ?>/community/staffs">Equipo</a>
					<a href="<?= $website_infos->lien; ?>/community/classements">Ranking</a>
					<?php elseif($webpage == 3) : ?>
					<a href="<?= $website_infos->lien; ?>/shop/diamants">Compre diamantes</a>
					<a href="<?= $website_infos->lien; ?>/shop/vip">VIP CLUB</a>
					<a href="<?= $website_infos->lien; ?>/shop/respect">Compre respetos</a>
					<a href="<?= $website_infos->lien; ?>/shop/badge">Compre placas</a>
					<?php endif; ?>
					<?php else : ?>
					<a href="<?= $website_infos->lien; ?>/index">Conectar</a>
					<a href="<?= $website_infos->lien; ?>/register">Crear cuenta</a>
					<?php endif; ?>
				</div>
			</div>
		</nav>
	</div><br>
	<div id="loaderspin">
		<div id="loader-wrapper">
			<div id="loader"></div>
			<div class="loader-section section-left"></div>
			<div class="loader-section section-right"></div>
		</div>
	</div>
<?php endif; ?>
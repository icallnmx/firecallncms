<?php
$pasdemtnc = 1;
require('global.php');
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta name="identifier-url" content="<?= $website_infos->lien; ?>"/>
		<meta name="reply-to" content="<?= $website_infos->email; ?>"/>
		<meta property="og:site_name" content="<?= $website_infos->nom; ?>"/>
		<meta property="og:title" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta property="og:url" content="<?= $website_infos->lien; ?>"/>
		<meta property="og:description" content="I&iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta property="og:image" content='http://makehabbo.fr/assets/img/meta.png'/>
		<meta name="description" content="&iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="keywords" content="habbox, habbo, virtuel, monde, réseau social, gratuit, communautée, avatar, chat, connectée, adolescence, jeu de rôle, rejoindre, social, groupes, forums, sécuritée, jouer, jeux, amis, rares, ados, jeunes, collector, collectionner, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, badges, musique, chat vip, fun, sortir, mmo, mmorpg, jeu massivement multijoueur, habbo, habboworld, habbodreams, jabbo, habbo hotel, habbo gratuit, habbo credit, habbocity, habbo-city, hbc, hcity, habbo city, bobba, bobbah hotel, bobbahotel, bobba hotel, bobba-hotel, jabbo, jabbo hotel, jabbonow, jabbohotel, jabborp, habbolove, habbo-love, habbo love, hlove, habbolove inscription, habbo, HABBO, habboo, retro habbo, rétro habbo, serveur habbo, retro, habbo retro gratuit, autre habbo, habbo autre, habbo retro qui marche bien, jeu comme habbo, jeux comme habbo, site comme habbo, habbo site, serveur privé habbo, habbo beta, hbeta, habbobeta, habbo-beta, habbo-dreams, habbo dreams, habbo dream, habbo-dreams, cola-hotel, cola hotel, bobbaworld, bobba-world, world, worldhabbo, world-habbo, habbiworld, habbo world, hworld, zunny, abbo, habbi, abboz, habboz, habbo gratuit, adohotel, adoh, ado-h, habbo credit, habbo hotel, habbo hotel gratuit, jouer a habbo gratuitement, habbo en gratuit, habbo retro, recrutement staff, recrutement, mmorpg, vip, animateur, animation, jeu du celib, clack ou smack, staff, rencontre, celibataire, casino, rares, magots, enable, boutique, fifa, foot, cheval, chevaux, piscine, crédits gratuits, crédit gratuit, staff club, virtuel, monde, réseau social, gratuit, communauté, avatar, chat, connecté, adolescence, jeu de rôle, rejoindre, social, groupes, forums, jouer, jeux, amis, ados, jeunes, collector, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, création, badges, musique, célébrité, chat vip, fun, sortir, mmo, chat, youtube, facebook, twitter"/>
		<title><?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!</title>
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/maintenance.css"/>
	</head>
	<body>
		<div class="header">
			<div class="center" style="background:url('https://hsource.fr/font/habbo_new/<?= $website_infos->nom; ?>.gif') no-repeat left center;">
				<form name="beta" method="post" action="<?= $website_infos->lien; ?>/req/beta.php">
					<input type="text" name="code" placeholder="Clave"/>
					<label for="go"></label>
					<input type="submit" name="go" value="Valider"/>
				</form>
			</div>
		</div>
		<div class="sub_header">
			<div class="center">
				<h1>Mantenimiento</h1>
				<br />
			</div>
		</div>
		<div class="page">
			<div class="center">
				<div class="left">
					<div class="box">
						<div class="title">
							<h1>Avances</h1>
							<p>Tenga en cuenta todo sobre el progreso</p>
						</div>
						<div class="content">
							<div class="bloc">
								<div class="image" style="background-image:url('<?= $website_infos->lien; ?>/public/images/3.png');"></div>
								<div class="bull">
									<h1>Progreso general</h1>
									<p>
										Hey! <?= $website_infos->nom; ?> Pronto abrira ve todo el progreso en nuestra pagina de facebook.
									</p>
								</div>
							</div>

							<div class="bloc">
								<div class="image" style="background-image:url('<?= $website_infos->lien; ?>/public/images/4.png');"></div>
								<div class="bull">
									<h1>Progreso del hotel</h1>
									<p>
										El hotel está listo al 95%, arreglando algunos errores restantes.
									</p>
								</div>
							</div>

							<div class="bloc">
								<div class="image" style="background-image:url('<?= $website_infos->lien; ?>/public/images/1.png');"></div>
								<div class="bull">
									<h1>Progreso del sitio</h1>
									<p>
										El desarrollo del sitio avanza rápidamente, se crearon algunas páginas principales.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="right">
					<div class="box">
						<div class="title">
							<h1>Facebook</h1>
							<p>Todas las novedades estan aqui</p>
						</div>
						<div class="content">
							<div id="fb-root"></div>
							<script>(function(d, s, id) {
							  var js, fjs = d.getElementsByTagName(s)[0];
							  if (d.getElementById(id)) return;
							  js = d.createElement(s); js.id = id;
							  js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.9";
							  fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));</script>
							<div class="fb-page" data-href="https://www.facebook.com/<?= $website_infos->facebook; ?>/" data-tabs="timeline" data-width="370" data-height="269" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
								<blockquote cite="https://www.facebook.com/<?= $website_infos->facebook; ?>" class="fb-xfbml-parse-ignore">
									<a href="https://www.facebook.com/<?= $website_infos->facebook; ?>">Carregamento de plugins do Facebook</a>
								</blockquote>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer">
			<div class="center">
				<div class="logo">
					<img src="<?= $website_infos->lien; ?>/public/images/footer.png">
				</div>
				<div class="links">
					<a href="https://www.facebook.com/<?= $website_infos->facebook; ?>/">Facebook</a>
				</div>
				<div class="copy">
					<?= $website_infos->nom; ?> en ningún caso pertenece a Habbo o Sulake Co.
				</div>
			</div>
		</div>

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script type="text/javascript" src="<?= $website_infos->lien; ?>/public/js/jquery.maintenance.js"></script>

	</body>
</html>
<?php
$website = $bdd->prepare('SELECT * FROM habboxcms_site WHERE id = :id');
$website->execute(['id' => '1']);
if($website->rowCount() == 1) {
	$website_infos = $website->fetch();
	if(!isset($pasdemtnc)) {
		if($website_infos->maintenance == 1) {
			if(isset($_SESSION['id'])) {
				if($session_infos->rank <= 5) {
					header('Location: /logout');
					exit();
				}
			} else {
				header('Location: /maintenance');
				exit();
			}
		}
	}
} else {
	die('Nenhum erro ocorreu (<b>#604105</b>), entre em contato com a Cypher clicando <a href="https://habbo-dev.fr/membre/4768-cypher/">aqui</a>.');
}
?>
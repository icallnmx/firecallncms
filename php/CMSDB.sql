CREATE TABLE `habboxcms_articles` (
  `id` int(11) NOT NULL,
  `titre` varchar(150) NOT NULL,
  `background` text NOT NULL,
  `contenu` text NOT NULL,
  `date_p` int(11) NOT NULL,
  `id_membre` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `habboxcms_a_comments`
--

CREATE TABLE `habboxcms_a_comments` (
  `id` int(11) NOT NULL,
  `id_article` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `commentaire` varchar(150) NOT NULL,
  `date_post` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `habboxcms_badges`
--

CREATE TABLE `habboxcms_badges` (
  `id` int(11) NOT NULL,
  `badge_id` varchar(25) NOT NULL,
  `prix` int(11) NOT NULL,
  `add_par` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `habboxcms_beta`
--

CREATE TABLE `habboxcms_beta` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cle` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `habboxcms_hotel`
--

CREATE TABLE `habboxcms_hotel` (
  `id` int(11) NOT NULL,
  `ip_vps` varchar(255) NOT NULL DEFAULT '42.42.42.42',
  `port` varchar(20) NOT NULL DEFAULT '30000',
  `ext_variables` varchar(2500) NOT NULL DEFAULT 'https://game.habbox.fr/game/gamedata/external_variables.txt',
  `ext_texts` varchar(2500) NOT NULL DEFAULT 'https://game.habbox.fr/game/gamedata/external_flash_texts.txt',
  `ext_override_variables` varchar(2500) NOT NULL DEFAULT 'https://game.habbox.fr/game/gamedata/override/external_override_variables.txt',
  `productdata` varchar(2500) NOT NULL DEFAULT 'https://game.habbox.fr/game/gamedata/productdata.txt',
  `furnidata` varchar(2500) NOT NULL DEFAULT 'https://game.habbox.fr/game/gamedata/furnidata13.xml',
  `ext_override_texts` varchar(2500) NOT NULL DEFAULT 'https://www.habbox.fr/game/gamedata/override/external_flash_override_texts.txt',
  `production` varchar(2500) NOT NULL DEFAULT 'https://game.habbox.fr/game/gordon/PRODUCTION-201707041014-428081343/',
  `production_swf` varchar(2500) NOT NULL DEFAULT 'https://game.habbox.fr/game/gordon/PRODUCTION-201707041014-428081343/HabboxTest5.swf'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `habboxcms_hotel`
--

INSERT INTO `habboxcms_hotel` (`id`, `ip_vps`, `port`, `ext_variables`, `ext_texts`, `ext_override_variables`, `productdata`, `furnidata`, `ext_override_texts`, `production`, `production_swf`) VALUES
(1, '42.42.42.42', '30000', 'https://game.habbox.fr/game/gamedata/external_variables.txt', 'https://game.habbox.fr/game/gamedata/external_flash_texts.txt', 'https://game.habbox.fr/game/gamedata/override/external_override_variables.txt', 'https://game.habbox.fr/game/gamedata/productdata.txt', 'https://game.habbox.fr/game/gamedata/furnidata13.xml', 'https://www.habbox.fr/game/gamedata/override/external_flash_override_texts.txt', 'https://game.habbox.fr/game/gordon/PRODUCTION-201707041014-428081343/', 'https://game.habbox.fr/game/gordon/PRODUCTION-201707041014-428081343/HabboxTest5.swf');

-- --------------------------------------------------------

--
-- Structure de la table `habboxcms_site`
--

CREATE TABLE `habboxcms_site` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL DEFAULT 'Habbo',
  `lien` varchar(255) NOT NULL DEFAULT 'http://localhost',
  `maintenance` enum('0','1') NOT NULL DEFAULT '0',
  `album1584` varchar(255) DEFAULT 'https://beta.habbopop.org/c_images/',
  `email` varchar(255) NOT NULL DEFAULT 'contact@habbo.fr',
  `twitter` varchar(255) NOT NULL DEFAULT '@HabboFR',
  `facebook` varchar(255) NOT NULL DEFAULT 'habbofrance',
  `facebook_pageid` varchar(255) NOT NULL DEFAULT '1654462784839080',
  `dedipass_public` varchar(255) NOT NULL,
  `dedipass_private` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `habboxcms_site`
--

INSERT INTO `habboxcms_site` (`id`, `nom`, `lien`, `maintenance`, `album1584`, `email`, `twitter`, `facebook`, `facebook_pageid`, `dedipass_public`, `dedipass_private`) VALUES
(1, 'Habbo', 'http://localhost', '1', 'https://beta.habbopop.org/c_images/album1584/', 'contact@habbo.fr', '@HabboFR', 'habboxfrance', '1654462784839080', 'c93024ffecafd807035156cf78f80c47', '1f0d91a25cb74a89e80038b212876551a195d971');

-- --------------------------------------------------------

--
-- Structure de la table `habboxcms_vipclub`
--

CREATE TABLE `habboxcms_vipclub` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time_restant` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `habboxcms_articles`
--
ALTER TABLE `habboxcms_articles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `habboxcms_a_comments`
--
ALTER TABLE `habboxcms_a_comments`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `habboxcms_badges`
--
ALTER TABLE `habboxcms_badges`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `habboxcms_beta`
--
ALTER TABLE `habboxcms_beta`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `habboxcms_hotel`
--
ALTER TABLE `habboxcms_hotel`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `habboxcms_site`
--
ALTER TABLE `habboxcms_site`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `habboxcms_vipclub`
--
ALTER TABLE `habboxcms_vipclub`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `habboxcms_articles`
--
ALTER TABLE `habboxcms_articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT pour la table `habboxcms_a_comments`
--
ALTER TABLE `habboxcms_a_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT pour la table `habboxcms_badges`
--
ALTER TABLE `habboxcms_badges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT pour la table `habboxcms_beta`
--
ALTER TABLE `habboxcms_beta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT pour la table `habboxcms_hotel`
--
ALTER TABLE `habboxcms_hotel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `habboxcms_site`
--
ALTER TABLE `habboxcms_site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `habboxcms_vipclub`
--
ALTER TABLE `habboxcms_vipclub`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `users`
ADD IF NOT EXISTS `fonction` VARCHAR(60) NOT NULL DEFAULT 'Membre',
ADD IF NOT EXISTS `disabled` ENUM('0', '1') NOT NULL DEFAULT '0';
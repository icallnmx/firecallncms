<?php
try {
    $host = "localhost"; // Host de mysql
    $nom_bdd = "callnmx"; //Nombre de base de datos.
    $user_bdd = "root"; // Usuario de base de datos.
    $mdp_bdd = ""; // Clave de base de datos.
   	$bdd = new PDO('mysql:host='.$host.';dbname='.$nom_bdd.';charset=utf8', ''.$user_bdd.'', ''.$mdp_bdd.'');
   	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $bdd->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
}

catch(Exception $e) {
	die('Error : ' . $e->getMessage());
}
?>
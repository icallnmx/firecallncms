<?php
session_start();

if(isset($_SESSION['id'])) {
	$account_id = $bdd->prepare('SELECT * FROM users WHERE id = :id');
	$account_id->execute(['id' => $_SESSION['id']]);
	if($account_id->rowCount() == 1) {
		$session_infos = $account_id->fetch();
		$_SESSION['id'] = $session_infos->id;
		$nouvelle_ip = $bdd->prepare('UPDATE users SET ip_last = :ip_last WHERE id = :id');
      	$nouvelle_ip->execute([
      		'ip_last' => $_SERVER['REMOTE_ADDR'],
      		'id' => $_SESSION['id']
      	]);
      	if($session_infos->disabled == 1) {
			header('Refresh:3; url=/logout');
			die('Su cuenta ha sido desactivada por un administrador.');
		}
	} else {
		header('Location: /logout');
		exit();
	}

	$ban_oupas = $bdd->prepare('SELECT id,expire FROM bans WHERE value = :value');
	$ban_oupas->execute(['value' => $session_infos->username]);
	if($ban_oupas->rowCount() > 0) {
		$ban_infos = $ban_oupas->fetch();
		if(time() > $ban_infos->expire) {
            $ban_delete = $bdd->prepare('DELETE FROM bans WHERE value = :value');
            $ban_delete->execute(['value' => $session_infos->username]);
        } else {
            header('Location: /logout');
            exit();
        }
	}

	$vipclub_c_fini = $bdd->prepare('SELECT * FROM habboxcms_vipclub WHERE time_restant < :time_restant');
	$vipclub_c_fini->execute(['time_restant' => time()]);
	if($vipclub_c_fini->rowCount() == 1) {
		$delete_vipclub = $bdd->prepare('DELETE FROM habboxcms_vipclub WHERE time_restant < :time_restant');
		$delete_vipclub->execute(['time_restant' => time()]);
		$update_account = $bdd->prepare('UPDATE users SET rank = :rank, fonction = :fonction WHERE id = :id');
		$update_account->execute(['rank' => "1", 'fonction' => "Membre", 'id' => $_SESSION['id']]);
	}
}
?>
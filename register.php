<?php
require('global.php');
if(isset($_SESSION['id'])) {
	header('Location: /me');
	exit();
}
?>
<!DOCTYPE html>
<html style="overflow-x:hidden;">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?= $website_infos->nom; ?>: &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos! </title>
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/sty-le.css">
		<link rel="stylesheet" href="<?= $website_infos->lien; ?>/public/themify-icons/themify-icons.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="Habbo" content="Habbo" />
		<meta name="twitter:card" content="summary"/>
		<meta name="twitter:site" content="@<?= $website_infos->twitter; ?>"/>
		<meta name="twitter:title" content="<?= $website_infos->nom; ?>: &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="twitter:description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="twitter:creator" content="@<?= $website_infos->twitter; ?>" />
		<meta name="twitter:image:src" content="https://i.imgur.com/jhQnyhw.png" />
		<meta name="twitter:domain" content="<?= $website_infos->lien; ?>"/>
		<meta name="identifier-url" content="<?= $website_infos->lien; ?>"/>
		<meta name="category" content="Rétro Habbo">
		<meta name="reply-to" content="<?= $website_infos->email; ?>">
		<meta property="og:site_name" content="<?= $website_infos->nom; ?> Hotel"/>
		<meta property="og:title" content="<?= $website_infos->nom; ?>: &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta property="og:url" content="<?= $website_infos->lien; ?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:description" content="<?= $website_infos->nom; ?> - f&iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta property="og:image" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:image:secure_url" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:locale" content="es_ES"/>
		<meta name="Author" content="Cypher, Shone"/>
		<meta name="description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="keywords" content="habbox, habbo, virtuel, monde, réseau social, gratuit, communautée, avatar, chat, connectée, adolescence, jeu de rôle, rejoindre, social, groupes, forums, sécuritée, jouer, jeux, amis, rares, ados, jeunes, collector, collectionner, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, badges, musique, chat vip, fun, sortir, mmo, mmorpg, jeu massivement multijoueur, habbo, habboworld, habbodreams, jabbo, habbo hotel, habbo gratuit, habbo credit, habbocity, habbo-city, hbc, hcity, habbo city, bobba, bobbah hotel, bobbahotel, bobba hotel, bobba-hotel, jabbo, jabbo hotel, jabbonow, jabbohotel, jabborp, habbolove, habbo-love, habbo love, hlove, habbolove inscription, habbo, HABBO, habboo, retro habbo, rétro habbo, serveur habbo, retro, habbo retro gratuit, autre habbo, habbo autre, habbo retro qui marche bien, jeu comme habbo, jeux comme habbo, site comme habbo, habbo site, serveur privé habbo, habbo beta, hbeta, habbobeta, habbo-beta, habbo-dreams, habbo dreams, habbo dream, habbo-dreams, cola-hotel, cola hotel, bobbaworld, bobba-world, world, worldhabbo, world-habbo, habbiworld, habbo world, hworld, zunny, abbo, habbi, abboz, habboz, habbo gratuit, adohotel, adoh, ado-h, habbo credit, habbo hotel, habbo hotel gratuit, jouer a habbo gratuitement, habbo en gratuit, habbo retro, recrutement staff, recrutement, mmorpg, vip, animateur, animation, jeu du celib, clack ou smack, staff, rencontre, celibataire, casino, rares, magots, enable, boutique, fifa, foot, cheval, chevaux, piscine, crédits gratuits, crédit gratuit, staff club, virtuel, monde, réseau social, gratuit, communauté, avatar, chat, connecté, adolescence, jeu de rôle, rejoindre, social, groupes, forums, jouer, jeux, amis, ados, jeunes, collector, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, création, badges, musique, célébrité, chat vip, fun, sortir, mmo, chat, youtube, facebook, twitter"/>
	</head>
	<body>
		<?php require_once('modeles/header.php'); ?>
			<div class="container-fluid content">
				<div class="container">
					<div id="resultat"></div>
					<div class="col-md-8">
						<div class="module-index">
							<h1>Registrarme</h1>
							<p style="position: relative;top:-3px;">Para entrar al hotel necesitamos alguna informacion...</p>
							<hr>
							<div class="content">
								<form class="form form--left registration-form" method="post">	
									<div class="password-new">
										<fieldset class="form__fieldset form__fieldset--box form__fieldset--box-top" email-address="registration.email">
											<div>
												<label for="bean_name" class="form__label">Usuario</label>
												<p style="margin:0px;top: -4px;">Su usuario puede contener letras mayusculas, minusculas e números.</p>
											</div>
											<div class="form__field">
												<input autofocus type="text" name="bean_name" id="bean_name" placeholder="Nombre de usuario" required="" class="form__input" value="">
											</div>
										</fieldset>
									</div>
									<fieldset class="form__fieldset form__fieldset--box form__fieldset--box-top" email-address="registration.email">
										<div>
											<label for="bean_name" class="form__label">E-mail</label>
											<p style="margin:0px;top: -4px;">Ponga un email valido para recurar su contraseña.</p>
										</div>
										<input type="email" name="bean_email" id="bean_email" placeholder="E-mail" required="" class="form__input" value="">
									</fieldset>
									<div class="password-new">
										<fieldset class="form__fieldset form__fieldset--box form__fieldset--box-top">
											<label for="password-new" class="form__label">Contraseña</label>
											<p style="margin:0px;top: -4px;">Su contraseña dene contener por lo menos 6 caracteres e incluir letras, y tambien números.</p>
											<div class="form__field">
												<input type="password" name="bean_password" id="bean_password" placeholder="Contraseña" type="password" required="" class="form__input">
											</div>
											<label for="password-new-repeated" class="form__label">Repita a contraseña</label>
											<p style="margin:0px;top: -4px;"></p>
											<div class="form__field">
												<input type="password" name="bean_repassword" id="bean_repassword" placeholder="Una vez mas" type="password" required="" class="form__input">
											</div>
										</fieldset>
									</div>

									<div class="form__footer">
										<input type="submit" class="form__rsubmit registration-form__button" id="submit" value="Crear Cuenta">
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="module-black">
							<h1 style="margin-top: 0px;">Twitter</h1>
							<p>Todas las notícias minuciosas de <?= $website_infos->nom; ?> en Twitter</p>
							<hr style="background-color: #232323;color:#232323;border-color: #323232;position: relative;top: -7px;">
							<div class="content" style="position: relative;top: -30px;">
								<a class="twitter-timeline" data-height="325" data-theme="light" data-link-color="#6a7c8c" href="https://twitter.com/<?= $website_infos->twitter; ?>">Tweets de <?= $website_infos->nom; ?></a>
								<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
								<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
							</div>
						</div>

						<div class="module-black" style="height: auto;">
							<h1>Facebook</h1>
							<p>Todas las notícias minuciosas de <?= $website_infos->nom; ?> en Facebook</p>
							<hr>
							<center>
								<iframe scrolling="no" src="../req/facebook.php" style="display: inline-block;height: 130px;border: none;margin-bottom: 15px;"></iframe>
							</center>
						</div>
					</div>

					<?php require_once('modeles/footer.php'); ?>

					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
					<script type="text/javascript">
					$(document).ready(function(){
						$("#submit").on('click', function(event){
							event.preventDefault();
							$.ajax({
								type: "POST",
								url: "<?= $website_infos->lien; ?>/req/register.php",
								data: "bean_name="+$("#bean_name").val()+"&bean_email="+$("#bean_email").val()+"&bean_password="+$("#bean_password").val()+"&bean_repassword="+$("#bean_repassword").val(),
								success: function(msg){
									if(msg == "ok") {
										document.location.href="<?= $website_infos->lien; ?>/me";
									}else {
										swal("Oops", msg,  "error");
									}
								}
							});
						});
						$("#loaderspin").css("display", "none");
					});
					</script>
				</div>
			</div>
		</div>
	</body>
</html>
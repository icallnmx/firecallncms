<?php
$webpage = 1;
require('global.php');
if(!isset($_SESSION['id'])) {
	header('Location: /index');
	exit();
}

if(isset($_GET['p']) AND !empty($_GET['p'])) {
	$p = htmlspecialchars($_GET['p']);
	if($p == "general") {
		$page = "general";
	} elseif($p == "mail") {
		$page = "mail";
	} elseif($p == "password") {
		$page = "password";
	} elseif($p == "tags") {
		$page = "tags";
	} else {
		$page = "general";
	}
} else {
	$page = "general";
}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?= $website_infos->nom; ?>: Mi configuracion</title>
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/sty-le.css">
		<link rel="stylesheet" href="<?= $website_infos->lien; ?>/public/themify-icons/themify-icons.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="Habbo" content="Habbo" />
		<meta name="twitter:card" content="summary"/>
		<meta name="twitter:site" content="@<?= $website_infos->twitter; ?>"/>
		<meta name="twitter:title" content="<?= $website_infos->nom; ?>: &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="twitter:description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="twitter:creator" content="@<?= $website_infos->twitter; ?>" />
		<meta name="twitter:image:src" content="https://i.imgur.com/jhQnyhw.png" />
		<meta name="twitter:domain" content="<?= $website_infos->lien; ?>"/>
		<meta name="identifier-url" content="<?= $website_infos->lien; ?>"/>
		<meta name="category" content="Rétro Habbo">
		<meta name="reply-to" content="<?= $website_infos->email; ?>">
		<meta property="og:site_name" content="<?= $website_infos->nom; ?> Hotel"/>
		<meta property="og:title" content="<?= $website_infos->nom; ?>: &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta property="og:url" content="<?= $website_infos->lien; ?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta property="og:image" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:image:secure_url" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:locale" content="es_ES"/>
		<meta name="Author" content="Cypher, Shone"/>
		<meta name="description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="keywords" content="habbox, habbo, virtuel, monde, réseau social, gratuit, communautée, avatar, chat, connectée, adolescence, jeu de rôle, rejoindre, social, groupes, forums, sécuritée, jouer, jeux, amis, rares, ados, jeunes, collector, collectionner, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, badges, musique, chat vip, fun, sortir, mmo, mmorpg, jeu massivement multijoueur, habbo, habboworld, habbodreams, jabbo, habbo hotel, habbo gratuit, habbo credit, habbocity, habbo-city, hbc, hcity, habbo city, bobba, bobbah hotel, bobbahotel, bobba hotel, bobba-hotel, jabbo, jabbo hotel, jabbonow, jabbohotel, jabborp, habbolove, habbo-love, habbo love, hlove, habbolove inscription, habbo, HABBO, habboo, retro habbo, rétro habbo, serveur habbo, retro, habbo retro gratuit, autre habbo, habbo autre, habbo retro qui marche bien, jeu comme habbo, jeux comme habbo, site comme habbo, habbo site, serveur privé habbo, habbo beta, hbeta, habbobeta, habbo-beta, habbo-dreams, habbo dreams, habbo dream, habbo-dreams, cola-hotel, cola hotel, bobbaworld, bobba-world, world, worldhabbo, world-habbo, habbiworld, habbo world, hworld, zunny, abbo, habbi, abboz, habboz, habbo gratuit, adohotel, adoh, ado-h, habbo credit, habbo hotel, habbo hotel gratuit, jouer a habbo gratuitement, habbo en gratuit, habbo retro, recrutement staff, recrutement, mmorpg, vip, animateur, animation, jeu du celib, clack ou smack, staff, rencontre, celibataire, casino, rares, magots, enable, boutique, fifa, foot, cheval, chevaux, piscine, crédits gratuits, crédit gratuit, staff club, virtuel, monde, réseau social, gratuit, communauté, avatar, chat, connecté, adolescence, jeu de rôle, rejoindre, social, groupes, forums, jouer, jeux, amis, ados, jeunes, collector, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, création, badges, musique, célébrité, chat vip, fun, sortir, mmo, chat, youtube, facebook, twitter"/>
	</head>
	<body>
		<?php require_once('modeles/header.php'); ?>
			<div class="container-fluid content">
				<div class="container">
					<div class="col-md-8">
						<div class="module-index">
							<h1 style="margin-left: 10px;">Parámetros</h1>
							<p style="position: relative;top:-3px;margin-left: 10px;">Aquí están los diferentes parámetros:</p>
							<hr>
							<div class="content" style="padding: 0 10px 10px 10px;">
								<?php if($page == "general") : ?>
								<form method="post" class="form form--left" style="display: inline-block;width: 100%;">
									<img src="https://cdn.discordapp.com/attachments/392042071982211072/452790651503902732/toolman.png" style="float: right;height: 100%;margin-top: 70px;">
									<fieldset class="form__fieldset form__fieldset--box" style="padding-left: 50px;">
										<h4>Pedidos de amistad</h4>
										<p style="margin-left: 0;top: 0;font-size:  14px;">De quién recibir pedidos de amigos:</p>
										<div class="form__field">
											<label class="form__label form__label--radiobutton">
											<input type="radio" class="form__radiobutton" id="online" name="online" value="1" <?php if($session_infos->block_newfriends == 1) : ?>checked<?php endif; ?> /> Nadie
										</div>
										<div class="form__field">
											<label class="form__label form__label--radiobutton">
											<input type="radio" class="form__radiobutton" id="online" name="online" value="0" <?php if($session_infos->block_newfriends == 0) : ?>checked<?php endif; ?>   /> Todo mundo<br><br>
										</div>
									</fieldset>

									<fieldset class="form__fieldset form__fieldset--box" style="padding-left: 50px;">
										<h4>Preferencia para seguirme</h4>
										<p style="margin-left: 0;top: 0;font-size:  14px;">Quien puede seguirme:</p>
										<div class="form__field">
											<label class="form__label form__label--radiobutton">
											<input type="radio" class="form__radiobutton" id="join" name="join" value="1"  <?php if($session_infos->hide_inroom == 1) : ?>checked<?php endif; ?> /> Nadie 
										</div>
										<div class="form__field">
											<label class="form__label form__label--radiobutton">
											<input type="radio" class="form__radiobutton" id="join" name="join" value="0"  <?php if($session_infos->hide_inroom == 0) : ?>checked<?php endif; ?>  /> Mis amigos<br><br>
										</div>
									</fieldset>
									<div class="form__footer">
										<button id="submitset1" class="form__submit">Guardar</button>
									</div>
								</form>
								<?php elseif($page == "mail") : ?>
								<form method="post" class="form form--left" style="display: inline-block;width: 100%;">
									<fieldset class="form__fieldset form__fieldset--box form__fieldset--box-bottom" style="margin-top: 0px;">
										<label for="password-current" class="form__label">Cambie su email</label>
										<p style="margin:0px;top: -4px;font-size:  14px;">Actualmente: <b id="actuMail"><a href="mailto:<?= $session_infos->mail; ?>"><?= $session_infos->mail; ?></a></b></p>
										<div class="form__field">
											<input type="email" name="mail" id="mail" placeholder="Introduzca su nueva dirección de correo electrónico aquí" required="" autocomplete="off" class="form__input">
										</div>
									</fieldset>
									<button id="submitset2" class="form__submit">Guardar</button>
								</form>
								<?php elseif($page == "password") : ?>
								<form method="post" class="form form--left" style="display: inline-block;width: 100%;">
									<habbo-password-new>
										<fieldset class="form__fieldset form__fieldset--box form__fieldset--box-top">
											<label for="password-current" class="form__label">Contraseña actual</label>
											<div class="form__field">
												<input type="password" name="currentPassword" id="currentPassword" placeholder="Contraseña actual" required="" autocomplete="off" class="form__input">
											</div>
										</fieldset>
										<fieldset class="form__fieldset form__fieldset--box form__fieldset--box-bottom">
											<label for="password-new-repeated" class="form__label">Nueva contraseña</label>
											<div class="form__field">
												<input type="password" name="newPassword" id="newPassword" placeholder="Nueva contraseña" required="" autocomplete="off" class="form__input">
											</div>
										</fieldset>
										<fieldset class="form__fieldset form__fieldset--box form__fieldset--box-bottom">
											<label for="password-new-repeated" class="form__label">Repita la nueva contraseña</label>
											<div class="form__field">
												<input type="password" name="retypedNewPassword" id="retypedNewPassword" placeholder="Una vez mas" required="" autocomplete="off" class="form__input">
											</div>
										</fieldset>
									</habbo-password-new>
									<div class="form__footer">
									<button type="submit" class="form__submit" id="submitset3">Guardar</button>
									</div>
								</form>
								<?php elseif($page == "tags") : ?>
								<form method="post" class="form form--left" style="display: inline-block;width: 100%;">
									<fieldset class="form__fieldset form__fieldset--box form__fieldset--box-top" style="margin-bottom: 0px;">
										<label for="password-current" class="form__label">Eliminar un tag <small style="font-size: 12px;"></small></label>
										<p style="margin:0px;top: -4px;font-size:  14px;">Haga clic en el tag para eliminarla</p>
										<div class="form__field tagss">
											<?php
											$mes_tags = $bdd->prepare('SELECT * FROM user_tags WHERE user_id = :user_id');
											$mes_tags->execute(['user_id' => $_SESSION['id']]);
											while($mes_tags_infos = $mes_tags->fetch()) {
											?>
											<div onclick="deletetag('<?= $mes_tags_infos->id; ?>')" style="border-radius: 5px;padding: 5px;background-color: #00000054;border-style: none;width: auto !important;padding: 14px;margin: 5px;color: white;display: inline-block;"><?= $mes_tags_infos->tag; ?></div>
											<?php } ?>
										</div>
									</fieldset>
								</form>

								<form method="post" class="form form--left" style="display: inline-block;width: 100%;">
									<fieldset class="form__fieldset form__fieldset--box form__fieldset--box-bottom" style="margin-top: 0px;">
										<label for="password-current" class="form__label">Agregar tag</label>
										<div class="form__field">
											<input type="text" name="tags" id="tags" placeholder="Digite su tag aqui" required="" autocomplete="off" class="form__input">
										</div>
									</fieldset>
									<button type="submit" class="form__submit" id="submitset4">Guardar</button>
								</form>
								<?php endif; ?>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="module-black" style="height: auto;">
							<h1 style="margin: 0px;padding: 15px;">Navegador</h1>
							<hr style="background-color: #232323;color:#232323;border-color: #323232;position: relative;margin: 0px;">
							<a class="navset navaset" href="<?= $website_infos->lien; ?>/settings">General</a>
							<a class="navset navaset" href="<?= $website_infos->lien; ?>/settings/mail">Modificar email</a>
							<a class="navset navaset" href="<?= $website_infos->lien; ?>/settings/password">Contraseña</a>
							<a class="navset navaset" href="<?= $website_infos->lien; ?>/settings/tags">Tags</a>
						</div>
					</div>

					<?php require_once('modeles/footer.php'); ?>

					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
					<script type="text/javascript">
					$(document).ready(function(){
						$("#submitset1").on('click', function(event){
							event.preventDefault();
		                    var join = $('input[type=radio][name=join]:checked').attr('value');
		                    var online = $('input[type=radio][name=online]:checked').attr('value');
							$.ajax({
								type: "POST",
								url: "<?= $website_infos->lien; ?>/req/settings.php",
								data: {'join': join, 'online' : online},
								success: function(msg){
									if(msg == "ok") {
										swal("Bien!", "Su configuracion se ha cambiado.",  "success");
									}else {
										swal("Oops", msg,  "error");
									}
								}
							});
						});
						$("#submitset2").on('click', function(event){
							event.preventDefault();
							$.ajax({
								type: "POST",
								url: "<?= $website_infos->lien; ?>/req/mail.php",
								data: "mail="+$("#mail").val(),
								success: function(msg){
									if(msg == "ok") {
										swal("Bien!", "Su email se ha cambiado.",  "success");
									}else {
										swal("Oops", msg,  "error");
									}
								}
							});
						});
						$("#submitset3").on('click', function(event){
							event.preventDefault();
							$.ajax({
								type: "POST",
								url: "<?= $website_infos->lien; ?>/req/password.php",
								data: "currentPassword="+$("#currentPassword").val()+"&newPassword="+$("#newPassword").val()+"&retypedNewPassword="+$("#retypedNewPassword").val(),
								success: function(msg){
									if(msg == "ok") {
										swal("Bien!", "Su contraseña se ha cambiado.",  "success");
									}else {
										swal("Oops", msg,  "error");
									}
								}
							});
						});
						$("#submitset4").on('click', function(event){
							event.preventDefault();
							$.ajax({
								type: "POST",
								url: "<?= $website_infos->lien; ?>/req/settings.php",
								data: "tags="+$("#tags").val(),
								success: function(msg){
									if(msg == "ok") {
										$(".tagss").load("tags .tagss");
										swal("Bien!", "Se ha agregado el tag.",  "success");
									}else {
										swal("Oops", msg,  "error");
									}
								}
							});
						});
						$("#loaderspin").css("display", "none");
					});
					function deletetag(id){
    					$.ajax({
        					url: "<?= $website_infos->lien; ?>/req/settings.php",
				            type: "POST",
				            data: {'id': id},                
				            success: function(data){
            					if(data == 'Success'){
									$(".tagss").load("tags .tagss");
                					swal("Bien!", "Se ha removido el tag.", "success");
             					}else{
                					swal("Oups", data, "error");
            					}                             
        					}
    					});
					}
					</script>
				</div>
			</div>
		</div>
	</body>
</html>
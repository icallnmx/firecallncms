<?php
$webpage = 2;
require('../global.php');
?>
<!DOCTYPE html>
<html style="overflow-x:hidden;">
	<head>
		<style>
		a:active, a:hover {
			outline: 0;
		}
		.ng-scope {
			width: 100%;
		}
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?= $website_infos->nom; ?>: Ranking</title>
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/sty-le.css">
		<link rel="stylesheet" href="<?= $website_infos->lien; ?>/public/themify-icons/themify-icons.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="Habbo" content="Habbo" />
		<meta name="twitter:card" content="summary"/>
		<meta name="twitter:site" content="@<?= $website_infos->twitter; ?>"/>
		<meta name="twitter:title" content="<?= $website_infos->nom; ?>: &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="twitter:description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="twitter:creator" content="@<?= $website_infos->twitter; ?>" />
		<meta name="twitter:image:src" content="https://i.imgur.com/jhQnyhw.png" />
		<meta name="twitter:domain" content="<?= $website_infos->lien; ?>"/>
		<meta name="identifier-url" content="<?= $website_infos->lien; ?>"/>
		<meta name="category" content="Rétro Habbo">
		<meta name="reply-to" content="<?= $website_infos->email; ?>">
		<meta property="og:site_name" content="<?= $website_infos->nom; ?> Hotel"/>
		<meta property="og:title" content="<?= $website_infos->nom; ?>: &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta property="og:url" content="<?= $website_infos->lien; ?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta property="og:image" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:image:secure_url" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:locale" content="es_ES"/>
		<meta name="Author" content="Cypher, Shone"/>
		<meta name="description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="keywords" content="habbox, habbo, virtuel, monde, réseau social, gratuit, communautée, avatar, chat, connectée, adolescence, jeu de rôle, rejoindre, social, groupes, forums, sécuritée, jouer, jeux, amis, rares, ados, jeunes, collector, collectionner, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, badges, musique, chat vip, fun, sortir, mmo, mmorpg, jeu massivement multijoueur, habbo, habboworld, habbodreams, jabbo, habbo hotel, habbo gratuit, habbo credit, habbocity, habbo-city, hbc, hcity, habbo city, bobba, bobbah hotel, bobbahotel, bobba hotel, bobba-hotel, jabbo, jabbo hotel, jabbonow, jabbohotel, jabborp, habbolove, habbo-love, habbo love, hlove, habbolove inscription, habbo, HABBO, habboo, retro habbo, rétro habbo, serveur habbo, retro, habbo retro gratuit, autre habbo, habbo autre, habbo retro qui marche bien, jeu comme habbo, jeux comme habbo, site comme habbo, habbo site, serveur privé habbo, habbo beta, hbeta, habbobeta, habbo-beta, habbo-dreams, habbo dreams, habbo dream, habbo-dreams, cola-hotel, cola hotel, bobbaworld, bobba-world, world, worldhabbo, world-habbo, habbiworld, habbo world, hworld, zunny, abbo, habbi, abboz, habboz, habbo gratuit, adohotel, adoh, ado-h, habbo credit, habbo hotel, habbo hotel gratuit, jouer a habbo gratuitieneent, habbo en gratuit, habbo retro, recrutieneent staff, recrutieneent, mmorpg, vip, animateur, animation, jeu du celib, clack ou smack, staff, rencontre, celibataire, casino, rares, magots, enable, boutique, fifa, foot, cheval, chevaux, piscine, crédits gratuits, crédit gratuit, staff club, virtuel, monde, réseau social, gratuit, communauté, avatar, chat, connecté, adolescence, jeu de rôle, rejoindre, social, groupes, forums, jouer, jeux, amis, ados, jeunes, collector, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, création, badges, musique, célébrité, chat vip, fun, sortir, mmo, chat, youtube, facebook, twitter"/>
	</head>
	<body>
		<?php require_once('../modeles/header.php'); ?>
			<div class="container-fluid content">
				<div class="container">
					<div class="col-md-4">
						<h1 style="margin-top: 0px;padding: 8px;font-size: 25px;background: #3a3a3a;color: white;">
							<center>Créditos</center>
						</h1>
						<?php
						$top_credits = $bdd->query('SELECT username,credits,look FROM users ORDER BY credits DESC LIMIT 0,5');
						while($top_credits_infos = $top_credits->fetch()) {
						?>
						<div class="col-md-12" style="padding: 8px;background: #3a3a3a;margin-bottom: 10px;">
							<div style="display: table;width: 100%;" class="ranking-user">
								<div class="habbo-imager" style="float: right;height: 106px;overflow:  hidden;background: url(<?= $website_infos->lien; ?>/public/images/1525799248.png);background-position: 101px 155px;margin: -8px;width: 94px;">
									<img src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $top_credits_infos->look; ?>&amp;size=b&amp;gesture=sml&amp;direction=2&amp;head_direction=2&amp;action=std" style="min-width: 54px;margin-top: -10px;margin-left: 28px;">
								</div>
								<h6 class="avatar__title" style="font-size:  13px;line-height: 70px;color:white;padding-left: 10px;"><?= $top_credits_infos->username; ?> tiene <?= $top_credits_infos->credits; ?> Créditos</h6>
							</div>
						</div>
						<?php } ?>
					</div>

					<div class="col-md-4">
						<h1 style="margin-top: 0px;padding: 8px;font-size: 25px;background: #c3c0be;color: white;">
							<center>Duckets</center>
						</h1>
						<?php
						$top_duckets = $bdd->query('SELECT username,activity_points,look FROM users ORDER BY activity_points DESC LIMIT 0,5');
						while($top_duckets_infos = $top_duckets->fetch()) {
						?>
						<div class="col-md-12" style="padding: 8px;background: #c3c0be;margin-bottom: 10px;">
							<div style="display: table;width: 100%;" class="ranking-user">
								<div class="habbo-imager" style="float: right;height: 106px;overflow:  hidden;background: url(<?= $website_infos->lien; ?>/public/images/1525801420.png);background-position: 101px 155px;margin: -8px;width: 94px;">
									<img src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $top_duckets_infos->look; ?>&amp;size=b&amp;gesture=sml&amp;direction=2&amp;head_direction=2&amp;action=std" style="min-width: 54px;margin-top: -10px;margin-left: 28px;">
								</div>
								<h6 class="avatar__title" style="font-size:  13px;line-height: 70px;color:white;padding-left: 10px;"><?= $top_duckets_infos->username; ?> tiene <?= $top_duckets_infos->activity_points; ?> duckets</h6>
							</div>
						</div>
						<?php } ?>
					</div>

					<div class="col-md-4">
						<h1 style="margin-top: 0px;padding: 8px;font-size: 25px;background: #3a3a3a;color: white;">
							<center><center>Diamantes</center>
						</h1>
						<?php
						$top_diamants = $bdd->query('SELECT username,vip_points,look FROM users ORDER BY vip_points DESC LIMIT 0,5');
						while($top_diamants_infos = $top_diamants->fetch()) {
						?>
						<div class="col-md-12" style="padding: 8px;background: #3a3a3a;margin-bottom: 10px;">
							<div style="display: table;width: 100%;" class="ranking-user">
								<div class="habbo-imager" style="float: right;height: 106px;overflow:  hidden;background: url(<?= $website_infos->lien; ?>/public/images/1525799248.png);background-position: 101px 155px;margin: -8px;width: 94px;">
									<img src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $top_diamants_infos->look; ?>&amp;size=b&amp;gesture=sml&amp;direction=2&amp;head_direction=2&amp;action=std" style="min-width: 54px;margin-top: -10px;margin-left: 28px;">
								</div>
								<h6 class="avatar__title" style="font-size:  13px;line-height: 70px;color:white;padding-left: 10px;"><?= $top_diamants_infos->username; ?> tiene <?= $top_diamants_infos->vip_points; ?> diamantes</h6>
							</div>
						</div>
						<?php } ?>
					</div>

					<div class="col-md-4">
						<h1 style="margin-top: 0px;padding: 8px;font-size: 25px;background: #c3c0be;color: white;">
							<center>Respetos</center>
						</h1>
						<?php
						$top_respects = $bdd->query('SELECT id,Respect FROM user_stats ORDER BY Respect DESC LIMIT 0,5');
						while($top_respects_infos = $top_respects->fetch()) {
						$account = $bdd->prepare('SELECT username,look FROM users WHERE id = :id');
						$account->execute(['id' => $top_respects_infos->id]);
						if($account->rowCount() == 1) {
							$account_infos = $account->fetch();
						?>
						<div class="col-md-12" style="padding: 8px;background: #c3c0be;margin-bottom: 10px;">
							<div style="display: table;width: 100%;" class="ranking-user">
								<div class="habbo-imager" style="float: right;height: 106px;overflow:  hidden;background: url(<?= $website_infos->lien; ?>/public/images/1525801420.png);background-position: 101px 155px;margin: -8px;width: 94px;">
									<img src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $account_infos->look; ?>&amp;size=b&amp;gesture=sml&amp;direction=2&amp;head_direction=2&amp;action=std" style="min-width: 54px;margin-top: -10px;margin-left: 28px;">
								</div>
								<h6 class="avatar__title" style="font-size:  13px;line-height: 70px;color:white;padding-left: 10px;"><?= $account_infos->username; ?>  Recibio <?= $top_respects_infos->Respect; ?> Respetos</h6>
							</div>
						</div>
						<?php }} ?>
					</div>

					<div class="col-md-4">
						<h1 style="margin-top: 0px;padding: 8px;font-size: 25px;background: #3a3a3a;color: white;">
							<center>Presentes recebidos</center>
						</h1>
						<?php
						$top_cadeaux = $bdd->query('SELECT id,GiftsReceived FROM user_stats ORDER BY GiftsReceived DESC LIMIT 0,5');
						while($top_cadeaux_infos = $top_cadeaux->fetch()) {
						$account = $bdd->prepare('SELECT username,look FROM users WHERE id = :id');
						$account->execute(['id' => $top_cadeaux_infos->id]);
						if($account->rowCount() == 1) {
							$account_infos = $account->fetch();
						?>
						<div class="col-md-12" style="padding: 8px;background: #3a3a3a;margin-bottom: 10px;">
							<div style="display: table;width: 100%;" class="ranking-user">
								<div class="habbo-imager" style="float: right;height: 106px;overflow:  hidden;background: url(<?= $website_infos->lien; ?>/public/images/1525799248.png);background-position: 101px 155px;margin: -8px;width: 94px;">
									<img src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $account_infos->look; ?>&amp;size=b&amp;gesture=sml&amp;direction=2&amp;head_direction=2&amp;action=std" style="min-width: 54px;margin-top: -10px;margin-left: 28px;">
								</div>
								<h6 class="avatar__title" style="font-size:  13px;line-height: 70px;color:white;padding-left: 10px;"><?= $account_infos->username; ?>  Recibio <?= $top_cadeaux_infos->GiftsReceived; ?> presentes</h6></a>
							</div>
						</div>
						<?php }} ?>
					</div>


					<div class="col-md-4">
						<h1 style="margin-top: 0px;padding: 8px;font-size: 25px;background: #c3c0be;color: white;">
							<center>Puntuacion ganada</center>
						</h1>
						<?php
						$top_winwins = $bdd->query('SELECT id,AchievementScore FROM user_stats ORDER BY AchievementScore DESC LIMIT 0,5');
						while($top_winwins_infos = $top_winwins->fetch()) {
						$account = $bdd->prepare('SELECT username,look FROM users WHERE id = :id');
						$account->execute(['id' => $top_winwins_infos->id]);
						if($account->rowCount() == 1) {
							$account_infos = $account->fetch();
						?>
						<div class="col-md-12" style="padding: 8px;background: #c3c0be;margin-bottom: 10px;">
							<div style="display: table;width: 100%;" class="ranking-user">
								<div class="habbo-imager" style="float: right;height: 106px;overflow:  hidden;background: url(<?= $website_infos->lien; ?>/public/images/1525801420.png);background-position: 101px 155px;margin: -8px;width: 94px;">
									<img src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $account_infos->look; ?>&amp;size=b&amp;gesture=sml&amp;direction=2&amp;head_direction=2&amp;action=std" style="min-width: 54px;margin-top: -10px;margin-left: 28px;">
								</div>
								<h6 class="avatar__title" style="font-size:  13px;line-height: 70px;color:white;padding-left: 10px;"><?= $account_infos->username; ?> tiene <?= $top_winwins_infos->AchievementScore; ?> Puntos</h6></a>
							</div>
						</div>
						<?php }} ?>
					</div>

					<?php require_once('../modeles/footer.php'); ?>
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
					<script type="text/javascript">
					$(document).ready(function(){
						$("#loaderspin").css("display", "none");
					});
					</script>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
$webpage = 2;
require('../global.php');
if(!isset($_SESSION['id'])) {
	header('Location: /index');
	exit();
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?= $website_infos->nom; ?>: Fotos</title>
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/sty-le.css">
		<link rel="stylesheet" href="<?= $website_infos->lien; ?>/public/themify-icons/themify-icons.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="Habbo" content="Habbo" />
		<meta name="twitter:card" content="summary"/>
		<meta name="twitter:site" content="@<?= $website_infos->twitter; ?>"/>
		<meta name="twitter:title" content="<?= $website_infos->nom; ?>: Entre dans un hôtel où tout est gratuit, fais-toi plein d'amis et deviens célèbre!"/>
		<meta name="twitter:description" content="<?= $website_infos->nom; ?> - fais-toi plein d'amis et deviens célèbre en séjournant GRATUITEMENT dans un Habbo totalement gratuit!"/>
		<meta name="twitter:creator" content="@<?= $website_infos->twitter; ?>" />
		<meta name="twitter:image:src" content="https://i.imgur.com/jhQnyhw.png" />
		<meta name="twitter:domain" content="<?= $website_infos->lien; ?>"/>
		<meta name="identifier-url" content="<?= $website_infos->lien; ?>"/>
		<meta name="category" content="Rétro Habbo">
		<meta name="reply-to" content="<?= $website_infos->email; ?>">
		<meta property="og:site_name" content="<?= $website_infos->nom; ?> Hôtel"/>
		<meta property="og:title" content="<?= $website_infos->nom; ?>: Crée ton avatar, décore ton appart, chatte et fais-toi plein d'amis."/>
		<meta property="og:url" content="<?= $website_infos->lien; ?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:description" content="<?= $website_infos->nom; ?> - fais-toi plein d'amis et deviens célèbre en séjournant GRATUITEMENT dans un Habbo totalement gratuit!"/>
		<meta property="og:image" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:image:secure_url" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:locale" content="fr_FR"/>
		<meta name="Author" content="Cypher, Shone"/>
		<meta name="description" content="<?= $website_infos->nom; ?> - Crée ton avatar Habbo, fais-toi plein d'amis et deviens célèbre en séjournant GRATUITEMENT dans un Habbo totalement gratuit!"/>
		<meta name="keywords" content="habbox, habbo, virtuel, monde, réseau social, gratuit, communautée, avatar, chat, connectée, adolescence, jeu de rôle, rejoindre, social, groupes, forums, sécuritée, jouer, jeux, amis, rares, ados, jeunes, collector, collectionner, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, badges, musique, chat vip, fun, sortir, mmo, mmorpg, jeu massivement multijoueur, habbo, habboworld, habbodreams, jabbo, habbo hotel, habbo gratuit, habbo credit, habbocity, habbo-city, hbc, hcity, habbo city, bobba, bobbah hotel, bobbahotel, bobba hotel, bobba-hotel, jabbo, jabbo hotel, jabbonow, jabbohotel, jabborp, habbolove, habbo-love, habbo love, hlove, habbolove inscription, habbo, HABBO, habboo, retro habbo, rétro habbo, serveur habbo, retro, habbo retro gratuit, autre habbo, habbo autre, habbo retro qui marche bien, jeu comme habbo, jeux comme habbo, site comme habbo, habbo site, serveur privé habbo, habbo beta, hbeta, habbobeta, habbo-beta, habbo-dreams, habbo dreams, habbo dream, habbo-dreams, cola-hotel, cola hotel, bobbaworld, bobba-world, world, worldhabbo, world-habbo, habbiworld, habbo world, hworld, zunny, abbo, habbi, abboz, habboz, habbo gratuit, adohotel, adoh, ado-h, habbo credit, habbo hotel, habbo hotel gratuit, jouer a habbo gratuitement, habbo en gratuit, habbo retro, recrutement staff, recrutement, mmorpg, vip, animateur, animation, jeu du celib, clack ou smack, staff, rencontre, celibataire, casino, rares, magots, enable, boutique, fifa, foot, cheval, chevaux, piscine, crédits gratuits, crédit gratuit, staff club, virtuel, monde, réseau social, gratuit, communauté, avatar, chat, connecté, adolescence, jeu de rôle, rejoindre, social, groupes, forums, jouer, jeux, amis, ados, jeunes, collector, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, création, badges, musique, célébrité, chat vip, fun, sortir, mmo, chat, youtube, facebook, twitter"/>
	</head>
	<body>
		<?php require_once('../modeles/header.php'); ?>
			<div class="container-fluid content">
				<div class="container">
					<?php
					$les_photos = $bdd->prepare('SELECT creator_id,file_name FROM camera_photos WHERE deleted = :deleted');
					$les_photos->execute(['deleted' => "0"]);
					while($les_photos_infos = $les_photos->fetch()) {
					$account = $bdd->prepare('SELECT username,look FROM users WHERE id = :id');
					$account->execute(['id' => $les_photos_infos->creator_id]);
					if($account->rowCount() == 1) {
					$account_infos = $account->fetch();
					?>
					<div class="col-md-4" style="padding: 5px;margin: 5px 15px;background: white;max-width: 330px;">
						<a href="<?= $website_infos->lien; ?>/home/<?= $account_infos->username; ?>">
							<img alt="" src="https://www.habbox.fr/camera/purchased/<?= $les_photos_infos->file_name; ?>.png">
						</a>

						<a href="<?= $website_infos->lien; ?>/home/<?= $account_infos->username; ?>" style="color: black;font-weight: 100;">
							<img alt="<?= $account_infos->username; ?>" width="54" height="62" src="https://avatar.habbo.digital/habbo-imaging/avatarimage?figure=<?= $account_infos->look; ?>&amp;headonly=1&amp;size=b&amp;gesture=sml&amp;direction=2&amp;head_direction=2&amp;action=std" style="min-width: 54px;">
							<h6 style="display: inline-block;color: black;font-weight:  bold;font-size:  17px;line-height:  50px;width: 75%;"><?= $account_infos->username; ?></h6>
						</a>
					</div>
					<?php }} ?>

					<?php require_once('../modeles/footer.php'); ?>
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
					<script>
					$(document).ready(function(){
						$("#loaderspin").css("display", "none");
					});
					</script>
				</div>
			</div>
		</div>
	</body>
</html>
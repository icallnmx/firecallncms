<?php
$webpage = 2;
require('../global.php');
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?= $website_infos->nom; ?>: Comunidad</title>
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/sty-le.css">
		<link rel="stylesheet" href="<?= $website_infos->lien; ?>/public/themify-icons/themify-icons.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="Habbo" content="Habbo" />
		<meta name="twitter:card" content="summary"/>
		<meta name="twitter:site" content="@<?= $website_infos->twitter; ?>"/>
		<meta name="twitter:title" content="<?= $website_infos->nom; ?>: &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="twitter:description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="twitter:creator" content="@<?= $website_infos->twitter; ?>" />
		<meta name="twitter:image:src" content="https://i.imgur.com/jhQnyhw.png" />
		<meta name="twitter:domain" content="<?= $website_infos->lien; ?>"/>
		<meta name="identifier-url" content="<?= $website_infos->lien; ?>"/>
		<meta name="category" content="Rétro Habbo">
		<meta name="reply-to" content="<?= $website_infos->email; ?>">
		<meta property="og:site_name" content="<?= $website_infos->nom; ?> Hotel"/>
		<meta property="og:title" content="<?= $website_infos->nom; ?>: &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta property="og:url" content="<?= $website_infos->lien; ?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta property="og:image" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:image:secure_url" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:locale" content="es_ES"/>
		<meta name="Author" content="Cypher, Shone"/>
		<meta name="description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="keywords" content="habbox, habbo, virtuel, monde, réseau social, gratuit, communautée, avatar, chat, connectée, adolescence, jeu de rôle, rejoindre, social, groupes, forums, sécuritée, jouer, jeux, amis, rares, ados, jeunes, collector, collectionner, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, badges, musique, chat vip, fun, sortir, mmo, mmorpg, jeu massivement multijoueur, habbo, habboworld, habbodreams, jabbo, habbo hotel, habbo gratuit, habbo credit, habbocity, habbo-city, hbc, hcity, habbo city, bobba, bobbah hotel, bobbahotel, bobba hotel, bobba-hotel, jabbo, jabbo hotel, jabbonow, jabbohotel, jabborp, habbolove, habbo-love, habbo love, hlove, habbolove inscription, habbo, HABBO, habboo, retro habbo, rétro habbo, serveur habbo, retro, habbo retro gratuit, autre habbo, habbo autre, habbo retro qui marche bien, jeu comme habbo, jeux comme habbo, site comme habbo, habbo site, serveur privé habbo, habbo beta, hbeta, habbobeta, habbo-beta, habbo-dreams, habbo dreams, habbo dream, habbo-dreams, cola-hotel, cola hotel, bobbaworld, bobba-world, world, worldhabbo, world-habbo, habbiworld, habbo world, hworld, zunny, abbo, habbi, abboz, habboz, habbo gratuit, adohotel, adoh, ado-h, habbo credit, habbo hotel, habbo hotel gratuit, jouer a habbo gratuitement, habbo en gratuit, habbo retro, recrutement staff, recrutement, mmorpg, vip, animateur, animation, jeu du celib, clack ou smack, staff, rencontre, celibataire, casino, rares, magots, enable, boutique, fifa, foot, cheval, chevaux, piscine, crédits gratuits, crédit gratuit, staff club, virtuel, monde, réseau social, gratuit, communauté, avatar, chat, connecté, adolescence, jeu de rôle, rejoindre, social, groupes, forums, jouer, jeux, amis, ados, jeunes, collector, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, création, badges, musique, célébrité, chat vip, fun, sortir, mmo, chat, youtube, facebook, twitter"/>
	</head>
	<body>
		<?php require_once('../modeles/header.php'); ?>
			<div class="container-fluid content">
				<div class="container">
					<div class="col-md-7">
						<div class="module-index">
							<h1>Últimas notícias</h1>
							<p style="position: relative;top:-3px;">Mantengase informado sobre los eventos actuales de <?= $website_infos->nom; ?></p>
							<hr>
							<div class="content">
								<?php $last_articles = $bdd->query('SELECT id,titre,background FROM habboxcms_articles ORDER BY date_p DESC LIMIT 0,8');
								while($last_articles_infos = $last_articles->fetch()) { ?>
								<a href="<?= $website_infos->lien; ?>/articles/<?= $last_articles_infos->id; ?>">
									<div class="bloc" style="margin-right: 10px;">
										<div class="image" style="background-image:url('<?= $last_articles_infos->background; ?>');">
											<div style=" background-color: rgba(0, 0, 0, 0.28);position: relative;top: 0;padding-top: 1px;padding-left: 10px;padding-bottom: 0px;width: 100%;right: 0;filter: dropshadow(color=#000,offx=1,offy=1);border-top-right-radius: 5px;border-top-left-radius: 5px;">
												<h3 style="color: white;width: auto;word-break: break-all;"><?= $last_articles_infos->titre; ?></h3>
											</div>
										</div>
									</div>
								</a>
								<?php } ?>
							</div>
						</div>
					</div>

					<div class="col-md-5">
						<div class="module-black" style="height: auto;display: inline-table;">
							<h1 style="margin-top: 0px;">Facebook</h1>
							<p>Todas las notícias minuciosas de  <?= $website_infos->nom; ?> en Facebook</p>
							<hr style="background-color: #232323;color:#232323;border-color: #323232;position: relative;top: -7px;">
							<center>
								<iframe scrolling="no" src="../req/facebook.php" style="display: inline-block;height: 130px;border: none;margin-bottom: 15px;"></iframe>
							</center>
						</div>
						<div class="module-black" style="height: auto;display: inline-table;margin-top: 20px;">
							<h1 style="margin-top: 0px;">Twitter</h1>
							<p>Todas las notícias minuciosas de  <?= $website_infos->nom; ?> en Twitter</p>
							<hr style="background-color: #232323;color:#232323;border-color: #323232;position: relative;top: -7px;">
							<div class="content" style="position: relative;top: -30px;">
								<a class="twitter-timeline" data-height="254" data-theme="light" data-link-color="#6a7c8c" href="https://twitter.com/<?= $website_infos->twitter; ?>">Tweets de <?= $website_infos->nom; ?> France</a>
								<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
								<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
							</div>
						</div>
					</div>

					<?php require_once('../modeles/footer.php'); ?>
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
					<script type="text/javascript">
					$(document).ready(function(){
						$("#loaderspin").css("display", "none");
					});
					</script>
				</div>
			</div>
		</div>
	</body>
</html>
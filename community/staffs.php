<?php
$webpage = 2;
require('../global.php');
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?= $website_infos->nom; ?>: Equipo</title>
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/sty-le.css">
		<link rel="stylesheet" href="<?= $website_infos->lien; ?>/public/themify-icons/themify-icons.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="Habbo" content="Habbo" />
		<meta name="twitter:card" content="summary"/>
		<meta name="twitter:site" content="@<?= $website_infos->twitter; ?>"/>
		<meta name="twitter:title" content="<?= $website_infos->nom; ?>: &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="twitter:description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="twitter:creator" content="@<?= $website_infos->twitter; ?>" />
		<meta name="twitter:image:src" content="https://i.imgur.com/jhQnyhw.png" />
		<meta name="twitter:domain" content="<?= $website_infos->lien; ?>"/>
		<meta name="identifier-url" content="<?= $website_infos->lien; ?>"/>
		<meta name="category" content="Rétro Habbo">
		<meta name="reply-to" content="<?= $website_infos->email; ?>">
		<meta property="og:site_name" content="<?= $website_infos->nom; ?> Hotel"/>
		<meta property="og:title" content="<?= $website_infos->nom; ?>: &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta property="og:url" content="<?= $website_infos->lien; ?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta property="og:image" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:image:secure_url" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:locale" content="es_ES"/>
		<meta name="Author" content="Cypher, Shone"/>
		<meta name="description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="keywords" content="habbox, habbo, virtuel, monde, réseau social, gratuit, communautée, avatar, chat, connectée, adolescence, jeu de rôle, rejoindre, social, groupes, forums, sécuritée, jouer, jeux, amis, rares, ados, jeunes, collector, collectionner, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, badges, musique, chat vip, fun, sortir, mmo, mmorpg, jeu massivement multijoueur, habbo, habboworld, habbodreams, jabbo, habbo hotel, habbo gratuit, habbo credit, habbocity, habbo-city, hbc, hcity, habbo city, bobba, bobbah hotel, bobbahotel, bobba hotel, bobba-hotel, jabbo, jabbo hotel, jabbonow, jabbohotel, jabborp, habbolove, habbo-love, habbo love, hlove, habbolove inscription, habbo, HABBO, habboo, retro habbo, rétro habbo, serveur habbo, retro, habbo retro gratuit, autre habbo, habbo autre, habbo retro qui marche bien, jeu comme habbo, jeux comme habbo, site comme habbo, habbo site, serveur privé habbo, habbo beta, hbeta, habbobeta, habbo-beta, habbo-dreams, habbo dreams, habbo dream, habbo-dreams, cola-hotel, cola hotel, bobbaworld, bobba-world, world, worldhabbo, world-habbo, habbiworld, habbo world, hworld, zunny, abbo, habbi, abboz, habboz, habbo gratuit, adohotel, adoh, ado-h, habbo credit, habbo hotel, habbo hotel gratuit, jouer a habbo gratuitement, habbo en gratuit, habbo retro, recrutement staff, recrutement, mmorpg, vip, animateur, animation, jeu du celib, clack ou smack, staff, rencontre, celibataire, casino, rares, magots, enable, boutique, fifa, foot, cheval, chevaux, piscine, crédits gratuits, crédit gratuit, staff club, virtuel, monde, réseau social, gratuit, communauté, avatar, chat, connecté, adolescence, jeu de rôle, rejoindre, social, groupes, forums, jouer, jeux, amis, ados, jeunes, collector, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, création, badges, musique, célébrité, chat vip, fun, sortir, mmo, chat, youtube, facebook, twitter"/>
	</head>
	<body>
		<?php require_once('../modeles/header.php'); ?>
			<div class="container-fluid content">
				<div class="container">
					<div class="col-md-8">
						<div class="col-md-12" style="padding: 0px">
							<div style="font-size: 22px;padding: 10px;color: #5b5b5b;background-color: #ffffff;">CEO</div>
						</div>
						<?php
						$les_admins = $bdd->prepare('SELECT username,look,fonction,motto,online FROM users WHERE rank = :rank');
						$les_admins->execute(['rank' => "15"]);
						while($les_admins_infos = $les_admins->fetch()) {
						?>
						<div class="col-md-6 ranking" style="padding: 15px 0px;margin: 15px 0px;background-color: #484746;border: solid #e9e9e9;border-width: 0px 3px 0px 0px;">
							<a href="<?= $website_infos->lien; ?>/home/<?= $les_admins_infos->username; ?>" class="staffbox">
								<div class="staff-member"> 
									<div class="staff-box">
										<div class="cols col-md-8" style="color: white;font-weight: bold;">
											<div class="titlestaff"><?= $les_admins_infos->username; ?>
												<div style="display: inline-block;width: 10px;height: 10px;background: <?php if($les_admins_infos->online == 1) echo 'green'; else echo 'red'; ?>;border-radius: 50px;"></div>
											</div>
											<div class="sub-title"></div>
											<div style="margin-bottom: 10px;color: white;position:  absolute;display: contents;"><?= htmlspecialchars(utf8_encode($les_admins_infos->motto)); ?></div>
											<div>
												<img src="<?= $website_infos->album1584; ?>ADM.gif">
											</div>
										</div>
										<div class="col-md-4" style="margin-top: -15px;margin-bottom: -15px;background:  url('<?= $website_infos->lien; ?>/public/images/1525799248.png');background-position: -13px -50px;position: relative;height: 142px;width: 33.33333333% !important;">
											<img style="position: relative;left: 39px;bottom: -17px;" src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $les_admins_infos->look; ?>&amp;head_direction=4&amp;gesture=sml&amp;size=m&amp;direction=4&amp;action=std">
										</div>
									</div>
								</div>
							</a>
						</div>
						<?php } ?>

						<div class="col-md-12" style="padding: 0px">
							<div style="font-size: 22px;padding: 10px;color: #5b5b5b;background-color: #ffffff;">Gerente</div>
						</div>
						<?php
						$les_anims = $bdd->prepare('SELECT username,look,fonction,motto,online FROM users WHERE rank = :rank');
						$les_anims->execute(['rank' => "14"]);
						while($les_anims_infos = $les_anims->fetch()) {
						?>
						<div class="col-md-6 ranking" style="padding: 15px 0px;margin: 15px 0px;background-color: #e2cf9f;border: solid #e9e9e9;border-width: 0px 3px 0px 0px;">
							<a href="<?= $website_infos->lien; ?>/home/<?= $les_anims_infos->username; ?>" class="staffbox">
								<div class="staff-member"> 
									<div class="staff-box">
										<div class="cols col-md-8" style="color: white;font-weight: bold;">
											<div class="titlestaff"><?= $les_anims_infos->username; ?>
												<div style="display: inline-block;width: 10px;height: 10px;background: <?php if($les_anims_infos->online == 1) echo 'green'; else echo 'red'; ?>;border-radius: 50px;"></div>
											</div>
											<div class="sub-title"></div>
											<div style="margin-bottom: 10px;color: white;position:  absolute;display: contents;"><?= htmlspecialchars(utf8_encode($les_anims_infos->motto)); ?></div>
											<div>
												<img src="<?= $website_infos->album1584; ?>ADM.gif">
											</div>
										</div>
										<div class="col-md-4" style="margin-top: -15px;margin-bottom: -15px;background:  url('<?= $website_infos->lien; ?>/public/images/1525801091.png');background-position: -13px -50px;position: relative;height: 142px;width: 33.33333333% !important;">
											<img style="position: relative;left: 39px;bottom: -17px;" src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $les_anims_infos->look; ?>&amp;head_direction=4&amp;gesture=sml&amp;size=m&amp;direction=4&amp;action=std">
										</div>
									</div>
								</div>
							</a>
						</div>
						<?php } ?>

						<div class="col-md-12" style="padding: 0px">
							<div style="font-size: 22px;padding: 10px;color: #5b5b5b;background-color: #ffffff;">Administrador</div>
						</div>
						<?php
						$les_modos = $bdd->prepare('SELECT username,look,fonction,motto,online FROM users WHERE rank = :rank');
						$les_modos->execute(['rank' => "13"]);
						while($les_modos_infos = $les_modos->fetch()) {
						?>
						<div class="col-md-6 ranking" style="padding: 15px 0px;margin: 15px 0px;background-color: #c3c0be;border: solid #e9e9e9;border-width: 0px 3px 0px 0px;">
							<a href="<?= $website_infos->lien; ?>/home/<?= $les_modos_infos->username; ?>" class="staffbox">
								<div class="staff-member"> 
									<div class="staff-box">
										<div class="cols col-md-8" style="color: white;font-weight: bold;">
											<div class="titlestaff"><?= $les_modos_infos->username; ?>
												<div style="display: inline-block;width: 10px;height: 10px;background: <?php if($les_modos_infos->online == 1) echo 'green'; else echo 'red'; ?>;border-radius: 50px;"></div>
											</div>
											<div class="sub-title"></div>
											<div style="margin-bottom: 10px;color: white;position:  absolute;display: contents;"><?= htmlspecialchars(utf8_encode($les_modos_infos->motto)); ?></div>
											<div>
												<img src="<?= $website_infos->album1584; ?>ADM.gif">
											</div>
										</div>
										<div class="col-md-4" style="margin-top: -15px;margin-bottom: -15px;background:  url('<?= $website_infos->lien; ?>/public/images/1525801420.png');background-position: -13px -50px;position: relative;height: 142px;width: 33.33333333% !important;">
											<img style="position: relative;left: 39px;bottom: -17px;" src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $les_modos_infos->look; ?>&amp;head_direction=4&amp;gesture=sml&amp;size=m&amp;direction=4&amp;action=std">
										</div>
									</div>
								</div>
							</a>
						</div>
						<?php } ?>

						<div class="col-md-12" style="padding: 0px">
							<div style="font-size: 22px;padding: 10px;color: #5b5b5b;background-color: #ffffff;">Moderador</div>
						</div>
						<?php
						$les_autres = $bdd->prepare('SELECT username,look,fonction,motto,online FROM users WHERE rank = :rank');
						$les_autres->execute(['rank' => "11"]);
						while($les_autres_infos = $les_autres->fetch()) {
						?>
						<div class="col-md-6 ranking" style="padding: 15px 0px;margin: 15px 0px;background-color: #b37b6b;border: solid #e9e9e9;border-width: 0px 3px 0px 0px;">
							<a href="<?= $website_infos->lien; ?>/home/<?= $les_autres_infos->username; ?>" class="staffbox">
								<div class="staff-member"> 
									<div class="staff-box">
										<div class="cols col-md-8" style="color: white;font-weight: bold;">
											<div class="titlestaff"><?= $les_autres_infos->username; ?>
												<div style="display: inline-block;width: 10px;height: 10px;background: <?php if($les_autres_infos->online == 1) echo 'green'; else echo 'red'; ?>;border-radius: 50px;"></div>
											</div>
											<div class="sub-title"></div>
											<div style="margin-bottom: 10px;color: white;position:  absolute;display: contents;"><?= htmlspecialchars(utf8_encode($les_autres_infos->motto)); ?></div>
											<div>
												<img src="<?= $website_infos->album1584; ?>ADM.gif">
											</div>
										</div>
										<div class="col-md-4" style="margin-top: -15px;margin-bottom: -15px;background:  url('<?= $website_infos->lien; ?>/public/images/1525800715.png');background-position: -13px -50px;position: relative;height: 142px;width: 33.33333333% !important;">
											<img style="position: relative;left: 39px;bottom: -17px;" src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $les_autres_infos->look; ?>&amp;head_direction=4&amp;gesture=sml&amp;size=m&amp;direction=4&amp;action=std">
										</div>
									</div>
								</div>
							</a>
						</div>
						<?php } ?>

						<div class="col-md-12" style="padding: 0px">
							<div style="font-size: 22px;padding: 10px;color: #5b5b5b;background-color: #ffffff;">GameMaster</div>
						</div>
						<?php
						$les_croupiers = $bdd->prepare('SELECT username,look,fonction,motto,online FROM users WHERE rank = :rank');
						$les_croupiers->execute(['rank' => "10"]);
						while($les_croupiers_infos = $les_croupiers->fetch()) {
						?>
						<div class="col-md-6 ranking" style="padding: 15px 0px;margin: 15px 0px;background-color: #484746;border: solid #e9e9e9;border-width: 0px 3px 0px 0px;">
							<a href="<?= $website_infos->lien; ?>/home/<?= $les_croupiers_infos->username; ?>" class="staffbox">
								<div class="staff-member"> 
									<div class="staff-box">
										<div class="cols col-md-8" style="color: white;font-weight: bold;">
											<div class="titlestaff"><?= $les_croupiers_infos->username; ?>
												<div style="display: inline-block;width: 10px;height: 10px;background: <?php if($les_croupiers_infos->online == 1) echo 'green'; else echo 'red'; ?>;border-radius: 50px;"></div>
											</div>
											<div class="sub-title"></div>
											<div style="margin-bottom: 10px;color: white;position:  absolute;display: contents;"><?= htmlspecialchars(utf8_encode($les_croupiers_infos->motto)); ?></div>
											<div>
												<img src="<?= $website_infos->album1584; ?>ADM.gif">
											</div>
										</div>
										<div class="col-md-4" style="margin-top: -15px;margin-bottom: -15px;background:  url('<?= $website_infos->lien; ?>/public/images/1525799248.png');background-position: -13px -50px;position: relative;height: 142px;width: 33.33333333% !important;">
											<img style="position: relative;left: 39px;bottom: -17px;" src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $les_croupiers_infos->look; ?>&amp;head_direction=4&amp;gesture=sml&amp;size=m&amp;direction=4&amp;action=std">
										</div>
									</div>
								</div>
							</a>
						</div>
						<?php } ?>

						<div class="col-md-12" style="padding: 0px">
							<div style="font-size: 22px;padding: 10px;color: #5b5b5b;background-color: #ffffff;">Arquitectos</div>
						</div>
						<?php
						$les_architectes = $bdd->prepare('SELECT username,look,fonction,motto,online FROM users WHERE rank = :rank');
						$les_architectes->execute(['rank' => "7"]);
						while($les_architectes_infos = $les_architectes->fetch()) {
						?>
						<div class="col-md-6 ranking" style="padding: 15px 0px;margin: 15px 0px;background-color: #e2cf9f;border: solid #e9e9e9;border-width: 0px 3px 0px 0px;">
							<a href="<?= $website_infos->lien; ?>/home/<?= $les_architectes_infos->username; ?>" class="staffbox">
								<div class="staff-member"> 
									<div class="staff-box">
										<div class="cols col-md-8" style="color: white;font-weight: bold;">
											<div class="titlestaff"><?= $les_architectes_infos->username; ?>
												<div style="display: inline-block;width: 10px;height: 10px;background: <?php if($les_architectes_infos->online == 1) echo 'green'; else echo 'red'; ?>;border-radius: 50px;"></div>
											</div>
											<div class="sub-title"></div>
											<div style="margin-bottom: 10px;color: white;position:  absolute;display: contents;"><?= htmlspecialchars(utf8_encode($les_architectes_infos->motto)); ?></div>
											<div>
												<img src="<?= $website_infos->album1584; ?>ADM.gif">
											</div>
										</div>
										<div class="col-md-4" style="margin-top: -15px;margin-bottom: -15px;background:  url('<?= $website_infos->lien; ?>/public/images/1525801091.png');background-position: -13px -50px;position: relative;height: 142px;width: 33.33333333% !important;">
											<img style="position: relative;left: 39px;bottom: -17px;" src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $les_architectes_infos->look; ?>&amp;head_direction=4&amp;gesture=sml&amp;size=m&amp;direction=4&amp;action=std">
										</div>
									</div>
								</div>
							</a>
						</div>
						<?php } ?>

						<div class="col-md-12" style="padding: 0px">
							<div style="font-size: 22px;padding: 10px;color: #5b5b5b;background-color: #ffffff;">Publicista</div>
						</div>
						<?php
						$les_pubeurs = $bdd->prepare('SELECT username,look,fonction,motto,online FROM users WHERE rank = :rank');
						$les_pubeurs->execute(['rank' => "4"]);
						while($les_pubeurs_infos = $les_pubeurs->fetch()) {
						?>
						<div class="col-md-6 ranking" style="padding: 15px 0px;margin: 15px 0px;background-color: #c3c0be;border: solid #e9e9e9;border-width: 0px 3px 0px 0px;">
							<a href="<?= $website_infos->lien; ?>/home/<?= $les_pubeurs_infos->username; ?>" class="staffbox">
								<div class="staff-member"> 
									<div class="staff-box">
										<div class="cols col-md-8" style="color: white;font-weight: bold;">
											<div class="titlestaff"><?= $les_pubeurs_infos->username; ?>
												<div style="display: inline-block;width: 10px;height: 10px;background: <?php if($les_pubeurs_infos->online == 1) echo 'green'; else echo 'red'; ?>;border-radius: 50px;"></div>
											</div>
											<div class="sub-title"></div>
											<div style="margin-bottom: 10px;color: white;position:  absolute;display: contents;"><?= htmlspecialchars(utf8_encode($les_pubeurs_infos->motto)); ?></div>
											<div>
												<img src="<?= $website_infos->album1584; ?>ADM.gif">
											</div>
										</div>
										<div class="col-md-4" style="margin-top: -15px;margin-bottom: -15px;background:  url('<?= $website_infos->lien; ?>/public/images/1525801420.png');background-position: -13px -50px;position: relative;height: 142px;width: 33.33333333% !important;">
											<img style="position: relative;left: 39px;bottom: -17px;" src="https://habbo.com/habbo-imaging/avatarimage?figure=<?= $les_pubeurs_infos->look; ?>&amp;head_direction=4&amp;gesture=sml&amp;size=m&amp;direction=4&amp;action=std">
										</div>
									</div>
								</div>
							</a>
						</div>
						<?php } ?>
					</div>

					<div class="col-md-4">
						<div class="module-black" style="height: auto;">
							<h1 style="margin-top:  0px;">Facebook</h1>
							<p>Todas las notícias minuciosas de <?= $website_infos->nom; ?> en Facebook</p>
							<hr>
							<center>
								<iframe scrolling="no" src="../req/facebook.php" style="display: inline-block;height: 130px;border: none;margin-bottom: 15px;"></iframe>
							</center>
						</div>

						<div class="module-black">
							<h1>Twitter</h1>
							<p>Todas las notícias minuciosas de <?= $website_infos->nom; ?> en Twitter</p>
							<hr style="background-color: #232323;color:#232323;border-color: #323232;position: relative;top: -7px;">
							<div class="content" style="position: relative;top: -30px;">
								<a class="twitter-timeline" data-height="283" data-theme="light" data-link-color="#6a7c8c" href="https://twitter.com/<?= $website_infos->twitter; ?>">Tweets de <?= $website_infos->nom; ?> France</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
								<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
							</div>
						</div>
					</div>

					<?php require_once('../modeles/footer.php'); ?>
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
					<script type="text/javascript">
					$(document).ready(function(){
						$("#loaderspin").css("display", "none");
					});
					</script>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
$webpage = 1;
require('global.php');
if(!isset($_SESSION['id'])) {
	header('Location: /index');
	exit();
}

if($session_infos->rank <= 7) {
	header('Location: /me');
	exit();
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?= $website_infos->nom; ?>: Crear noticia</title>
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/sty-le.css">
		<link rel="stylesheet" href="<?= $website_infos->lien; ?>/public/themify-icons/themify-icons.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<link rel="stylesheet" href="https://cdn.wysibb.com/css/default/wbbtheme.css" type="text/css" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php require_once('modeles/header.php'); ?>
			<div class="container-fluid content">
				<div class="container">
					<div class="col-md-8">
						<div class="module-index">
							<h1 style="margin-left: 10px;">Crear noticia</h1>
							<hr>
							<div class="content" style="padding: 0 10px 10px 10px;">
								<form method="post" class="form form--left" style="display: inline-block;width: 100%;">
									<fieldset class="form__fieldset form__fieldset--box form__fieldset--box-bottom" style="margin-top: 0px;">
										<label for="password-current" class="form__label">Título</label>
										<div class="form__field">
											<input type="text" name="titre" id="titre" placeholder="Digite el título de la notícia aqui" required="" maxlength="150" autocomplete="off" class="form__input">
										</div>

										<label for="password-current" class="form__label">Imagen (Fondo)</label>
										<div class="form__field">
											<input type="text" name="img" id="img" placeholder="Url de imagen" required="" autocomplete="off" class="form__input">
										</div>

										<label for="password-current" class="form__label">Contenido</label>
										<div class="form__field">
											<textarea type="text" name="contenu" id="contenu" rows="8">Digite el contenido de la noticia</textarea>
										</div>
									</fieldset><br>
									<button id="submit" type="submit" class="form__submit">Crear notícia</button>
								</form>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="module-black" style="height: auto;">
							<h1 style="margin: 0px;padding: 15px;">Navegacion</h1>
							<hr style="background-color: #232323;color:#232323;border-color: #323232;position: relative;margin: 0px;">
							<?php if($session_infos->rank >= 8) : ?>
							<a class="navset navaset" href="<?= $website_infos->lien; ?>/admin_article">Crear noticia</a>
							<?php endif; ?>
							<?php if($session_infos->rank == 7 || $session_infos->rank >= 9) : ?>
							<a class="navset navaset" href="<?= $website_infos->lien; ?>/admin_ban">Banear usuario</a>
							<a class="navset navaset" href="<?= $website_infos->lien; ?>/admin_desac">Desactivar una cuenta</a>
							<?php endif; ?>
						</div>
					</div>

					<?php require_once('modeles/footer.php'); ?>

					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
					<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
					<script src="https://cdn.wysibb.com/js/jquery.wysibb.min.js"></script>
					<script type="text/javascript">
					$(document).ready(function() {
						$("#submit").on('click', function(event){
					        event.preventDefault();
					        $.ajax({
					            type: "POST",
					            url: "<?= $website_infos->lien; ?>/req/article.php",
					            data: "titre="+$("#titre").val()+"&img="+$("#img").val()+"&contenu="+$("#contenu").bbcode(),
					            success: function(msg){
					                if(msg == "ok") {
					                    swal("Bien!", "Su noticia ahora es visible en el sitio.",  "success");
					                }else {
					                    swal("Oops", msg,  "error");
					                }
					            }
					        });
					    });
					    $("#loaderspin").css("display", "none");
					});
					$(function() {
						$("#contenu").wysibb();
					});
					</script>
				</div>
			</div>
		</div>
	</body>
</html>
jQuery(function() {
	// beta key
	jQuery(document).on('submit', 'form', function() {
		var tthis = jQuery(this)
		  , action = tthis.attr('action')
		  , code = jQuery('> input[type="text"]', tthis).val()
		  , body = jQuery('body');

		jQuery('> .error', body).remove();

		jQuery.post(action, { code : code }, function(data) {
			if (data.error) {
				body.prepend('<div class="error">' + data.message + '</div>');
			} else {
				window.location = data.location;
			}
		}, 'json');

		return false;
	})
});
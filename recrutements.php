<?php
require('global.php');
?>
<!DOCTYPE html>

<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?= $website_infos->nom; ?>: Reclutamiento</title>
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/sty-le.css">
		<link rel="stylesheet" href="<?= $website_infos->lien; ?>/public/themify-icons/themify-icons.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="Habbo" content="Habbo" />
		<meta name="twitter:card" content="summary"/>
		<meta name="twitter:site" content="@<?= $website_infos->twitter; ?>"/>
		<meta name="twitter:title" content="<?= $website_infos->nom; ?>: &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="twitter:description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="twitter:creator" content="@<?= $website_infos->twitter; ?>" />
		<meta name="twitter:image:src" content="https://i.imgur.com/jhQnyhw.png" />
		<meta name="twitter:domain" content="<?= $website_infos->lien; ?>"/>
		<meta name="identifier-url" content="<?= $website_infos->lien; ?>"/>
		<meta name="category" content="Rétro Habbo">
		<meta name="reply-to" content="<?= $website_infos->email; ?>">
		<meta property="og:site_name" content="<?= $website_infos->nom; ?> Hotel"/>
		<meta property="og:title" content="<?= $website_infos->nom; ?>: &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta property="og:url" content="<?= $website_infos->lien; ?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta property="og:image" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:image:secure_url" content="https://i.imgur.com/jhQnyhw.png" />
		<meta property="og:locale" content="es_ES"/>
		<meta name="Author" content="Cypher, Shone"/>
		<meta name="description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
		<meta name="keywords" content="habbox, habbo, virtuel, monde, réseau social, gratuit, communautée, avatar, chat, connectée, adolescence, jeu de rôle, rejoindre, social, groupes, forums, sécuritée, jouer, jeux, amis, rares, ados, jeunes, collector, collectionner, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, badges, musique, chat vip, fun, sortir, mmo, mmorpg, jeu massivement multijoueur, habbo, habboworld, habbodreams, jabbo, habbo hotel, habbo gratuit, habbo credit, habbocity, habbo-city, hbc, hcity, habbo city, bobba, bobbah hotel, bobbahotel, bobba hotel, bobba-hotel, jabbo, jabbo hotel, jabbonow, jabbohotel, jabborp, habbolove, habbo-love, habbo love, hlove, habbolove inscription, habbo, HABBO, habboo, retro habbo, rétro habbo, serveur habbo, retro, habbo retro gratuit, autre habbo, habbo autre, habbo retro qui marche bien, jeu comme habbo, jeux comme habbo, site comme habbo, habbo site, serveur privé habbo, habbo beta, hbeta, habbobeta, habbo-beta, habbo-dreams, habbo dreams, habbo dream, habbo-dreams, cola-hotel, cola hotel, bobbaworld, bobba-world, world, worldhabbo, world-habbo, habbiworld, habbo world, hworld, zunny, abbo, habbi, abboz, habboz, habbo gratuit, adohotel, adoh, ado-h, habbo credit, habbo hotel, habbo hotel gratuit, jouer a habbo gratuitement, habbo en gratuit, habbo retro, recrutement staff, recrutement, mmorpg, vip, animateur, animation, jeu du celib, clack ou smack, staff, rencontre, celibataire, casino, rares, magots, enable, boutique, fifa, foot, cheval, chevaux, piscine, crédits gratuits, crédit gratuit, staff club, virtuel, monde, réseau social, gratuit, communauté, avatar, chat, connecté, adolescence, jeu de rôle, rejoindre, social, groupes, forums, jouer, jeux, amis, ados, jeunes, collector, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, création, badges, musique, célébrité, chat vip, fun, sortir, mmo, chat, youtube, facebook, twitter"/>
	</head>
	<body>
		<?php require_once('modeles/header.php'); ?>
			<div class="container-fluid content">
				<div class="container">
					<div class="col-md-6">
						<div class="module-index">
							<h1>
								<center>Publicista</center>
							</h1>
							<hr>
							<div class="content">
								Publicista profesional tiene como objetivo visitar otros servidores retro o redes sociales para promover Habbo e invitar a nuevos usuarios a ir allí. Debe saber cómo usar herramientas de publicidad como el Macro flood, que no es obligatorio, pero se recomienda encarecidamente, esto aligerará su trabajo.
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="module-index">
							<h1>
								<center>Arquitecto</center>
							</h1>
							<hr>
							<div class="content">
								El arquitecto es una persona que contribuye a la construcción de eventos y lugares públicos que puede encontrar en el hotel. Su comportamiento debe reflejar su trabajo, debe ser respetuoso, agradable y fiel. Dará todo para representar a su equipo y su trabajo.
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="module-index">
							<h1>
								<center>Moderador</center>
							</h1>
							<hr>
							<div class="content">
								Un moderador es una persona que tiene una buena relación con los jugadores del hotel, a menudo debe estar activo dentro del hotel, además de poseer los conceptos básicos de las herramientas, lo que también requiere darse cuenta de que esta no es una posición para tomarse a la ligera.
					 		</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="module-index">
							<h1>
								<center>Diseñador</center>
							</h1>
							<hr>
							<div class="content">
								Un diseñador gráfico es un profesional de la comunicación que diseña soluciones de comunicación visual. Es el responsable de hacer las imágenes para el hotel, todo tipo de: insignias, pancartas, mobis, ropa, imágenes para redes ...
Trabaja en diferentes programas para hacer más atractiva la imagen del hotel.
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="module-index">
							<h1>
								<center>GameMaster</center>
							</h1>
							<hr>
							<div class="content">
								Un GameMaster es una persona encargada de animar el hotel. ¡Tendrá una buena imaginación y un buen conocimiento de la tecnología cableada para poder ofrecer juegos de calidad a todos los jugadores! Puede organizar minijuegos, patrocinar jugadores, crear eventos, etc.
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="module-index">
							<h1>
								<center>Embajador</center>
							</h1>
							<hr>
							<div class="content">
								Los embajadores actúan como asesores de nuevos usuarios y ayudan a preservar la comunidad en salas públicas. Los embajadores son usuarios que han sido elegidos por su sentido común y experiencia, y que han mostrado un interés natural en ayudar a otros usuarios de la comunidad.
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="module-index">
							<div class="content" style="padding:8px;">
								<select id="recrute" onchange="recrut()">
									<option value="" selected>Elige la posición que más te convenga.</option>
									<option value="Pubeur pro">Publicista</option>
									<option value="Architecte">Arquitecto</option>
									<option value="Croupier">Moderador</option>
									<option value="Graphiste">Diseñador</option>
									<option value="Animateur">GameMaster</option>
									<option value="Ambassadeur">Embajador</option>
								</select>
								<div id="form"></div>
							</div>
						</div>
					</div>

					<?php require_once('modeles/footer.php'); ?>
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
					<script>
					$(document).ready(function(){
						$("#loaderspin").css("display", "none");
					});
					function recrut() {
					    var poste = document.getElementById("recrute").value;
					    if(poste == "Pubeur pro") {
					        $("#form").empty();
					        $("#form").append("<a href=\"https://docs.google.com/forms/d/e/1FAIpQLSfp2RSYrFmQ4g-OJOmYfBYGUn0AFawFPPs7YsNbuSZiBhVM0g/viewform\" target=\"_blank\" class=\"form__submit2\">Acceder al formulario</a>");
					    }
					    if(poste == "Architecte") {
					        $("#form").empty();
					        $("#form").append("<a href=\"https://docs.google.com/forms/d/e/1FAIpQLScLNS-qEah8khjeuW4TS35bF2GkOBomGkySLtPc5fAP97DMbQ/viewform\" target=\"_blank\" class=\"form__submit2\">Acceder al formulario</a>");
					    }
					    if(poste == "Croupier") {
					        $("#form").empty();
					        $("#form").append("<a href=\"https://docs.google.com/forms/d/e/1FAIpQLSdIx2NqUThbHJtMb8mEbNZKRENEAsxCnWwD3yOt-bospcP4NA/viewform\" target=\"_blank\" class=\"form__submit2\">Acceder al formulario</a>");
					    }
					    if(poste == "Graphiste") {
					        $("#form").empty();
					        $("#form").append("<a href=\"https://docs.google.com/forms/d/e/1FAIpQLSfPdA8Sp0YL7o3XjcU_FSqcDIH5ByJU6W10DljVufVLqQEpug/viewform\" target=\"_blank\" class=\"form__submit2\">Acceder al formulario</a>");
					    }
					    if(poste == "Animateur") {
					        $("#form").empty();
					        $("#form").append("<a href=\"https://docs.google.com/forms/d/e/1FAIpQLSegPz8P4bDa-YhAB-K8Px_3j3MK1XVaoeXNyvFErr1zNMJ8sg/viewform\" target=\"_blank\" class=\"form__submit2\">Acceder al formulario</a>");
					    }
					    if(poste == "Ambassadeur") {
					        $("#form").empty();
					        $("#form").append("<a href=\"https://docs.google.com/forms/d/e/1FAIpQLSeRQmPJv_DXSeKQ9W21CUqtVbksL7eiyJC3rKlUiRWzbviPhw/viewform\" target=\"_blank\" class=\"form__submit2\">Acceder al formulario</a>");
					    }
					};
					</script>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
$webpage = 3;
require('../global.php');

$vipclub_j_restants = $bdd->prepare('SELECT * FROM habboxcms_vipclub WHERE user_id = :user_id');
$vipclub_j_restants->execute(['user_id' => $_SESSION['id']]);
if($vipclub_j_restants->rowCount() == 1) {
	$i = $vipclub_j_restants->fetch();
}

if(isset($_GET['do']) AND !empty($_GET['do'])) {
	$do = htmlspecialchars($_GET['do']);
	if($do == "buy") {
		if($vipclub_j_restants->rowCount() == 0) {
			if($session_infos->vip_points >= 100) {
				$update_account = $bdd->prepare('UPDATE users SET vip_points = vip_points - :vip_points, rank = :rank, credits = credits + :credits, activity_points = activity_points + :activity_points, fonction = :fonction WHERE id = :id');
				$update_account->execute([
					'vip_points' => "100",
					'rank' => "2",
					'credits' => "80000",
					'activity_points' => "80000",
					'fonction' => "VIP Club",
					'id' => $_SESSION['id']
				]);
				$new_vipclub = $bdd->prepare('INSERT INTO habboxcms_vipclub (user_id, time_restant) VALUES (:user_id, :time_restant)');
				$new_vipclub->execute([
					'user_id' => $_SESSION['id'],
					'time_restant' => strtotime('+1 month')
				]);
				$insert_badges = $bdd->prepare('INSERT INTO user_badges (user_id, badge_id) VALUES (:user_id, :badge_id)');
				$insert_badges->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => "COM58"
				]);
				$insert_badges->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => "FR722"
				]);
				$insert_badges->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => "ES700"
				]);
				$insert_badges->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => "UK162"
				]);
				$insert_badges->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => "BPB01"
				]);
				$insert_badges->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => "PAR15"
				]);
				$insert_badges->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => "IT235"
				]);
				$insert_badges->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => "GVIP"
				]);
				$insert_badges->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => "NT046"
				]);
				$insert_badges->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => "UK178"
				]);
				$insert_badges->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => "UK242"
				]);
				$insert_badges->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => "VIPD"
				]);
				$insert_badges->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => "JNG09"
				]);
				$insert_badges->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => "US0G"
				]);
				$insert_badges->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => "UK660"
				]);
				$insert_badges->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => "PT198"
				]);
				$_SESSION['ok'] = "Usted ahora es un miembro de VIP Club.";
			} else {
				$_SESSION['erreur'] = "Usted no tiene diamantes suficientes.";
			}
		} else {
			$_SESSION['erreur'] = "Usted todavía es un miembro de VIP Club.";
		}
	}
}
?>
<!DOCTYPE html>
<html lang="nl-NL">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?= $website_infos->nom; ?>: VIP Club</title>
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/sty-le.css">
		<link rel="stylesheet" href="<?= $website_infos->lien; ?>/public/themify-icons/themify-icons.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	</head>
	<body>
		<?php if(isset($_SESSION['ok'])) { ?>
		<script type="text/javascript">swal("Bravo !", "<?= $_SESSION['ok']; ?>", "success")</script>
		<?php unset($_SESSION['ok']); } elseif(isset($_SESSION['erreur'])) { ?>
		<script type="text/javascript">swal("Error !", "<?= $_SESSION['erreur']; ?>", "error")</script>
		<?php unset($_SESSION['erreur']); } ?>
		<?php require_once('../modeles/header.php'); ?>
			<div class="container-fluid content">
				<div class="container">
					<div class="col-md-8" style="padding: 0px;">
						<div class="col-md-12">
							<div style="color: #6e6e6e;padding: 8px;background: #ffffff;margin-bottom: 10px;">
								<b style="color: #6e6e6e;font-size:  19px;padding: 0px 0px 10px 0px;display: block;font-weight:  bold;">VIP</b>
								<hr style="margin-top:0px;">
								Cuando usted se une al club VIP, usted tiene muchas más ventajas que si usted era un jugador simple, usted también ganaría el respeto de los demás! Todo por sólo <a style="color: #585b5e;font-weight: bold;">100</a> diamantes por mes.</p>
							</div>
						</div>

						<div class="col-md-8">
							<div style="display: inline-grid;width: 100%;margin-bottom: 15px;">
								<div class="boxtitle" style="font-size: 22px;padding: 10px;color: #6e6e6e;background-color: white;"> Comandos
									<p style="font-size: 15px;color: #6e6e6e;"><b>→</b><b> :fastwalk</b> que permite andar rapidamente<br></p>
									<p style="font-size: 15px;color: #6e6e6e;"><b>→</b><b> :myteleport</b> que te permite teleportar<br></p>
									<p style="font-size: 15px;color: #6e6e6e;"><b>→</b><b> :myoverride</b> que te permite andar por todas partes<br></p>
									<p style="font-size: 15px;color: #6e6e6e;"><b>→</b><b> :flagme</b> que permite que cambies de nombre<br></p>
									<p style="font-size: 15px;color: #6e6e6e;"><b>→</b><b> :spull</b> que permite empujar de lejos el usuario<br></p>
									<p style="font-size: 15px;color: #6e6e6e;"><b>→</b><b> :spush</b> que empuja al usuario lejos<br></p>
									<p style="font-size: 15px;color: #6e6e6e;"><b>→</b><b> :fenable</b> que mantiene un enable para siempre<br></p>
									<p style="font-size: 15px;color: #6e6e6e;"><b>→</b><b> :bubble</b> que mantiene una burbuja para siempre<br></p>
								</div>
							</div>

							<div style="padding: 8px;background: #3a3a3a;margin-bottom: 10px;">
								<center>
									<b style="color: white;font-size:  19px;padding: 0px 0px 10px 0px;display: block;">16 placas</b>
								</center>
								<p style="margin: 0;">
									<img src="<?= $website_infos->lien; ?>/public/badges/COM58.gif">
									<img src="<?= $website_infos->lien; ?>/public/badges/FR722.gif">
									<img src="<?= $website_infos->lien; ?>/public/badges/ES700.gif">
									<img src="<?= $website_infos->lien; ?>/public/badges/UK162.gif">
									<img src="<?= $website_infos->lien; ?>/public/badges/BPB01.gif">
									<img src="<?= $website_infos->lien; ?>/public/badges/PAR15.gif">
									<img src="<?= $website_infos->lien; ?>/public/badges/IT235.gif">
									<img src="<?= $website_infos->lien; ?>/public/badges/GVIP.gif">
									<img src="<?= $website_infos->lien; ?>/public/badges/NT046.gif">
									<img src="<?= $website_infos->lien; ?>/public/badges/UK178.gif">
									<img src="<?= $website_infos->lien; ?>/public/badges/UK242.gif">
									<img src="<?= $website_infos->lien; ?>/public/badges/VIPD.gif">
									<img src="<?= $website_infos->lien; ?>/public/badges/JNG09.gif">
									<img src="<?= $website_infos->lien; ?>/public/badges/US0G.gif">
									<img src="<?= $website_infos->lien; ?>/public/badges/UK660.gif">
									<img src="<?= $website_infos->lien; ?>/public/badges/PT198.gif">
								</p>
							</div>
						</div>

						<div class="col-md-4"> 
							<div style="padding: 8px;background: #3a3a3a;margin-bottom: 10px;text-align:  center;    height: 144px;">
								<img src="<?= $website_infos->lien; ?>/public/images/1526367298.png" style="zoom: 1.5;image-rendering:  pixelated;">
								<p style="font-size: 21px;font-weight:  bold;color: white;margin: 0;">1 categoria</p><p style="font-size: 21px;font-weight:  bold;color: white;margin: 0;"> 10 subcategorias</p>
							</div>
						 
							<div style="padding: 8px;background: #3a3a3a;margin-bottom: 10px;text-align:  center;    height: 144px;">
								<img src="https://i.goopics.net/wDA24.png" style="zoom: 4;image-rendering:  pixelated;">
								<p style="font-size: 21px;font-weight:  bold;color: white;margin: 0;">80.000 créditos</p><p style="font-size: 21px;font-weight:  bold;color: white;margin: 0;"> 80.000 billetes</p>
							</div>

							<div style="padding: 8px;background: #3a3a3a;margin-bottom: 10px;text-align:  center;    height: 144px;">
								<img src="<?= $website_infos->lien; ?>/public/images/1526369134.ico" style="zoom: 2.5;image-rendering:  pixelated;">
								<p style="font-size: 21px;font-weight:  bold;color: white;margin: 0;">1 enable</p>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="module-black" style="height: auto;">
							<h1 style="margin: 0px;padding: 15px;">Cartera</h1>
							<hr style="background-color: #232323;color:#232323;border-color: #323232;position: relative;margin: 0px;">
							<div class="pcredit ranking-user" >
								<div class="purse__item purse__item--credits" style="color: white;padding: 15px;"><?= $session_infos->credits; ?> créditos</div>
							</div>
							<div class="pdiams ranking-user" >
								<div class="purse__item purse__item--diamonds" style="color: white;padding: 15px;"><?= $session_infos->vip_points; ?> diamantes</div>
							</div>

							<div class="pducket ranking-user" >
								<div class="purse__item purse__item--ducket" style="color: white;padding: 15px;"><?= $session_infos->activity_points; ?> duckets</div>
							</div>
							<div class="pvip ranking-user" >
								<div class="purse__item purse__item--vip" style="color: white;padding: 15px;">Tú aún tienes <?php if($vipclub_j_restants->rowCount() == 0) : echo '0'; else : echo ceil(abs($i->time_restant - time()) / 86400); endif; ?> dias(s) de subscripcion VIP</div>
							</div>
						</div>
						<a href="<?= $website_infos->lien; ?>/shop/vip?do=buy" id="vip" class="form__submit3">Comprar VIP Club</a>
					</div>

					<?php require_once('../modeles/footer.php'); ?>
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
					<script type="text/javascript">
					$(document).ready(function(){
						$("#loaderspin").css("display", "none");
					});
					</script>
				</div>
			</div>
		</div>
	</body>
</html>
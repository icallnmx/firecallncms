<?php
$webpage = 3;
require('../global.php');

$vipclub_j_restants = $bdd->prepare('SELECT * FROM habboxcms_vipclub WHERE user_id = :user_id');
$vipclub_j_restants->execute(['user_id' => $_SESSION['id']]);
if($vipclub_j_restants->rowCount() == 1) {
	$i = $vipclub_j_restants->fetch();
}

if(isset($_GET['do']) AND !empty($_GET['do'])) {
	$do = htmlspecialchars($_GET['do']);
	if($do == "buy") {
		if($session_infos->vip_points >= 90) {
			$update_account = $bdd->prepare('UPDATE users SET vip_points = vip_points - :vip_points WHERE id = :id');
			$update_account->execute([
				'vip_points' => "90",
				'id' => $_SESSION['id']
			]);
			$update_respects = $bdd->prepare('UPDATE user_stats SET DailyRespectPoints = DailyRespectPoints + :DailyRespectPoints WHERE id = :id');
			$update_respects->execute([
				'DailyRespectPoints' => "100",
				'id' => $_SESSION['id']
			]);
			$_SESSION['ok'] = "Su compra ha sido validada.";
		} else {
			$_SESSION['erreur'] = "Usted no tiene diamantes suficientes.";
		}
	} elseif($do == "buy2") {
		if($session_infos->vip_points >= 50) {
			$update_account = $bdd->prepare('UPDATE users SET vip_points = vip_points - :vip_points WHERE id = :id');
			$update_account->execute([
				'vip_points' => "50",
				'id' => $_SESSION['id']
			]);
			$update_respects = $bdd->prepare('UPDATE user_stats SET DailyRespectPoints = DailyRespectPoints + :DailyRespectPoints WHERE id = :id');
			$update_respects->execute([
				'DailyRespectPoints' => "50",
				'id' => $_SESSION['id']
			]);
			$_SESSION['ok'] = "Su compra ha sido validada.";
		} else {
			$_SESSION['erreur'] = "Usted no tiene diamantes suficientes.";
		}
	} elseif($do == "buy3") {
		if($session_infos->vip_points >= 170) {
			$update_account = $bdd->prepare('UPDATE users SET vip_points = vip_points - :vip_points WHERE id = :id');
			$update_account->execute([
				'vip_points' => "170",
				'id' => $_SESSION['id']
			]);
			$update_respects = $bdd->prepare('UPDATE user_stats SET DailyRespectPoints = DailyRespectPoints + :DailyRespectPoints WHERE id = :id');
			$update_respects->execute([
				'DailyRespectPoints' => "200",
				'id' => $_SESSION['id']
			]);
			$_SESSION['ok'] = " Su compra ha sido validada..";
		} else {
			$_SESSION['erreur'] = "Usted no tiene diamantes suficientes.";
		}
	} elseif($do == "buy4") {
		if($session_infos->vip_points >= 350) {
			$update_account = $bdd->prepare('UPDATE users SET vip_points = vip_points - :vip_points WHERE id = :id');
			$update_account->execute([
				'vip_points' => "350",
				'id' => $_SESSION['id']
			]);
			$update_respects = $bdd->prepare('UPDATE user_stats SET DailyRespectPoints = DailyRespectPoints + :DailyRespectPoints WHERE id = :id');
			$update_respects->execute([
				'DailyRespectPoints' => "500",
				'id' => $_SESSION['id']
			]);
			$_SESSION['ok'] = " Su compra ha sido validada..";
		} else {
			$_SESSION['erreur'] = "Usted no tiene diamantes suficientes.";
		}
	}
}
?>
<!DOCTYPE html>
<html lang="nl-NL">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?= $website_infos->nom; ?>: Comprar respetos</title>
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/sty-le.css">
		<link rel="stylesheet" href="<?= $website_infos->lien; ?>/public/themify-icons/themify-icons.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	</head>
	<body>
		<?php if(isset($_SESSION['ok'])) { ?>
		<script type="text/javascript">swal("Bravo !", "<?= $_SESSION['ok']; ?>", "success")</script>
		<?php unset($_SESSION['ok']); } elseif(isset($_SESSION['erreur'])) { ?>
		<script type="text/javascript">swal("Erreur !", "<?= $_SESSION['erreur']; ?>", "error")</script>
		<?php unset($_SESSION['erreur']); } ?>
		<?php require_once('../modeles/header.php'); ?>
			<div class="container-fluid content">
				<div class="container">
					<div class="col-md-8">
						<div class="module-index">
							<h1>La compra de respetos</h1>
							<hr>
							<div class="content">
								La compra de costes de respeto entre <b>50 a 350 diamantes</b> que permite respetar a los jugadores del hotel!
								</br></br>
								En primer lugar, usted tiene que conectarse al hotel, luego entrar en una habitación (cualquiera) y entonces usted elige el jugador que desea respetar, usted hace clic en este jugador y una lista de acciones se abre. En esta lista, usted podrá ver la interacción: "respeto"!
							</div>
						</div>

						<div class="module-black" style="height: auto;display: table;">
							<h1 style="margin-top: 0px;">Respetos</h1>
							<hr style="background-color: #232323;color:#232323;border-color: #323232;position: relative;top: -7px;">
							<div class="content" style="position: relative;top: -30px;">
							   <br></br><a href="respect?do=buy2" class="form__submit" style="display: block;margin-bottom: 15px;">Compre 50 respetos por 50 <img src="<?= $website_infos->lien; ?>/public/images/icon_661.png"></a>
							   <br></br><a href="respect?do=buy" class="form__submit" style="display: block;margin-bottom: 15px;">Compre 100 respetos por 90 <img src="<?= $website_infos->lien; ?>/public/images/icon_661.png"></a>
							   <br></br><a href="respect?do=buy3" class="form__submit" style="display: block;margin-bottom: 15px;">Compre 200 respetos por 170 <img src="<?= $website_infos->lien; ?>/public/images/icon_661.png"></a>
							   <br></br><a href="respect?do=buy4" class="form__submit" style="display: block;margin-bottom: 15px;">Compre 500 respetos por 350 <img src="<?= $website_infos->lien; ?>/public/images/icon_661.png"></a> 
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="module-black" style="height: auto;">
							<h1 style="margin: 0px;padding: 15px;">Cartera</h1>
							<hr style="background-color: #232323;color:#232323;border-color: #323232;position: relative;margin: 0px;">
							<div class="pcredit ranking-user" >
								<div class="purse__item purse__item--credits" style="color: white;padding: 15px;"><?= $session_infos->credits; ?> créditos</div>
							</div>
							<div class="pdiams ranking-user" >
								<div class="purse__item purse__item--diamonds" style="color: white;padding: 15px;"><?= $session_infos->vip_points; ?> diamantes</div>
							</div>

							<div class="pducket ranking-user" >
								<div class="purse__item purse__item--ducket" style="color: white;padding: 15px;"><?= $session_infos->activity_points; ?> duckets</div>
							</div>
							<div class="pvip ranking-user" >
								<div class="purse__item purse__item--vip" style="color: white;padding: 15px;">Tú aún tienes <?php if($vipclub_j_restants->rowCount() == 0) : echo '0'; else : echo ceil(abs($i->time_restant - time()) / 86400); endif; ?> dias(s) de subscripcion VIP</div>
							</div>
						</div>
					</div>

					<?php require_once('../modeles/footer.php'); ?>
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
					<script type="text/javascript">
					$(document).ready(function(){
						$("#loaderspin").css("display", "none");
					});
					</script>
				</div>
			</div>
		</div>
	</body>
</html>
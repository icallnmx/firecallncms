<?php
$webpage = 3;
require('../global.php');

$vipclub_j_restants = $bdd->prepare('SELECT * FROM habboxcms_vipclub WHERE user_id = :user_id');
$vipclub_j_restants->execute(['user_id' => $_SESSION['id']]);
if($vipclub_j_restants->rowCount() == 1) {
	$i = $vipclub_j_restants->fetch();
}
?>
<!DOCTYPE html>
<html lang="es-ES">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?= $website_infos->nom; ?>: Comprar placas</title>
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/sty-le.css">
		<link rel="stylesheet" href="<?= $website_infos->lien; ?>/public/themify-icons/themify-icons.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	</head>
	<body>
		<?php require_once('../modeles/header.php'); ?>
			<div class="container-fluid content">
				<div class="container">
					<div class="col-md-8">
						<div class="module-black" style="height: auto;display: table;">
							<h1 style="margin-top: 0px;">Placas</h1>
							<p>Placas a la venta</p>
							<hr style="background-color: #232323;color:#232323;border-color: #323232;position: relative;top: -7px;">
							<div class="content" style="position: relative;top: -30px;">
								<div class="badgeachat">
									<?php
									$les_badges = $bdd->query('SELECT * FROM habboxcms_badges ORDER BY id DESC');
									while($les_badges_infos = $les_badges->fetch()) {
									?>
									<form method="post" style="display: inline-block;width:  24%;height: 114px;">
										<div onclick="buybadge('<?= $les_badges_infos->badge_id; ?>')" class="form__submit"  style="margin-bottom: 15px;text-align: center;height: 100%;">
											<img src="<?= $website_infos->album1584; ?><?= $les_badges_infos->badge_id; ?>.gif" style="margin: 5px auto;display: block;">
											<span class="payment-methods__label">
												<b><?= $les_badges_infos->prix; ?> diamantes</b>
											</span>
										</div>
									</form>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>


					<div class="col-md-4">
						<div class="module-black" style="height: auto;">
							<h1 style="margin: 0px;padding: 15px;">Cartera</h1>
							<hr style="background-color: #232323;color:#232323;border-color: #323232;position: relative;margin: 0px;">
							<div class="pcredit ranking-user" >
								<div class="purse__item purse__item--credits" style="color: white;padding: 15px;"><?= $session_infos->credits; ?> créditos</div>
							</div>
							<div class="pdiams ranking-user" >
								<div class="purse__item purse__item--diamonds" style="color: white;padding: 15px;"><?= $session_infos->vip_points; ?> diamantes</div>
							</div>

							<div class="pducket ranking-user" >
								<div class="purse__item purse__item--ducket" style="color: white;padding: 15px;"><?= $session_infos->activity_points; ?> duckets</div>
							</div>
							<div class="pvip ranking-user" >
								<div class="purse__item purse__item--vip" style="color: white;padding: 15px;">Tú aún tienes <?php if($vipclub_j_restants->rowCount() == 0) : echo '0'; else : echo ceil(abs($i->time_restant - time()) / 86400); endif; ?> dias(s) de subscripcion VIP</div>
							</div>
						</div>
					</div>

					<?php require_once('../modeles/footer.php'); ?>
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
					<script type="text/javascript">
					$(document).ready(function(){
						$("#loaderspin").css("display", "none");
					});
					function buybadge(badgeid){
    					$.ajax({
        					url: "<?= $website_infos->lien; ?>/req/badge.php",
				            type: "POST",
				            data: {'badgeid': badgeid},                
				            success: function(data){
            					if(data == 'Success'){
									$(".badgeachat").load("badge .badgeachat");
                					swal("Bien!", "Usted ha comprado la placa.", "success");
             					}else{
                					swal("Oups", data, "error");
            					}                             
        					}
    					});
					}
					</script>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
$webpage = 3;
require('../global.php');

$vipclub_j_restants = $bdd->prepare('SELECT * FROM habboxcms_vipclub WHERE user_id = :user_id');
$vipclub_j_restants->execute(['user_id' => $_SESSION['id']]);
if($vipclub_j_restants->rowCount() == 1) {
	$i = $vipclub_j_restants->fetch();
}
?>
<!DOCTYPE html>
<html lang="es-ES">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?= $website_infos->nom; ?>: Tienda</title>
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/sty-le.css">
		<link rel="stylesheet" href="<?= $website_infos->lien; ?>/public/themify-icons/themify-icons.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	</head>
	<body>
		<?php require_once('../modeles/header.php'); ?>
			<div class="container-fluid content">
				<div class="container">
					<div class="col-md-8">
						<div class="module-black" style="height: auto;display: inline-block;background: #505050;">
							<h1 style="margin-top: 0px;">Comprar diamantes</h1>
							<p>Usted tiene <?= $session_infos->vip_points; ?> diamante(s)</p>
							<hr style="background-color: #232323;color:#232323;border-color: #323232;position: relative;top: -7px;">
							<div class="content" style="position: relative;top: -30px;">
								<p>
									<center>
										<?php if(isset($_SESSION['achat']) AND !empty($_SESSION['achat'])) { ?>
										<div class="alert alert-success alert-success" style="display: block;">
                                            Su código de acesso está incorreto
                                        </div>
                                    	<?php unset($_SESSION['achat']); } ?>
										<div data-dedipass="dea6f4198f31343a2b9f2ca4e1b4133f" data-dedipass-custom=""></div>
									</center>	
								</p>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="module-black" style="height: auto;margin-bottom:  15px;">
							<h1 style="margin: 0px;padding: 15px;">Cartera</h1>
							<hr style="background-color: #232323;color:#232323;border-color: #323232;position: relative;margin: 0px;">
							<div class="pcredit ranking-user" >
								<div class="purse__item purse__item--credits" style="color: white;padding: 15px;"><?= $session_infos->credits; ?> créditos</div>
							</div>
							<div class="pdiams ranking-user" >
								<div class="purse__item purse__item--diamonds" style="color: white;padding: 15px;"><?= $session_infos->vip_points; ?> diamantes</div>
							</div>

							<div class="pducket ranking-user" >
								<div class="purse__item purse__item--ducket" style="color: white;padding: 15px;"><?= $session_infos->activity_points; ?> duckets</div>
							</div>
							<div class="pvip ranking-user" >
								<div class="purse__item purse__item--vip" style="color: white;padding: 15px;">Tú aún tienes <?php if($vipclub_j_restants->rowCount() == 0) : echo '0'; else : echo ceil(abs($i->time_restant - time()) / 86400); endif; ?> dias(s) de subscripcion VIP</div>
							</div>
						</div>
						<div class="module-index">
							<h1>Comprando diamantes</h1>
							<hr>
							<div class="content">
								Cuando usted compra los diamantes, usted compra <b>150</b> que permite comprar valores raros (extras, ultras, supremo ..), únase al Club VIP y compre puntos de honor o puntuacion!
								</br></br>
								Los diamantes le sirven para comprar en el hotel y en el sitio.
								</br></br>
								Los diamantes son la única moneda que paga en <?= $website_infos->nom; ?>.
							</div>
						</div>
					</div>

					<script src="//api.dedipass.com/v1/pay.js"></script>
					<?php require_once('../modeles/footer.php'); ?>
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
					<script type="text/javascript">
					$(document).ready(function(){
						$("#loaderspin").css("display", "none");
					});
					</script>
				</div>
			</div>
		</div>
	</body>
</html>
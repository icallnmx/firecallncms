<!DOCTYPE html>
<!-- saved from url=(0023)http://hreino.us/client -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="author" content="Genaro">
<link rel="shortcut icon" type="image/x-icon" href="http://hreino.us/themes/Slopt/assets/images/favicon.png">
<link rel="author" href="http://hreino.us/">
<link rel="publisher" href="http://hreino.us/">
<link rel="canonical" href="http://hreino.us/">
<meta property="og:type" content="Juego">
<meta name="generator" content="http://hreino.us">
<meta property="og:title" content="HReino Hotel">
<meta property="og:description" content="Crea tu personaje, construye tu habitación, conversa y haz nuevos amigos!">
<meta property="og:image" content="http://hreino.us/themes/Slopt/assets/images/ogimg.png">
<meta property="og:url" content="http://hreino.us">
<meta property="og:site_name" content="Hotel - HReino - ¡Crea tu personaje, construye tu habitación, conversa y haz nuevos amigos!">

<script type="text/javascript">
    shortcut={all_shortcuts:{},add:function(a,b,c){var d={type:"keydown",propagate:!1,disable_in_input:!1,target:document,keycode:!1};if(c)for(var e in d)"undefined"==typeof c[e]&&(c[e]=d[e]);else c=d;d=c.target,"string"==typeof c.target&&(d=document.getElementById(c.target)),a=a.toLowerCase(),e=function(d){d=d||window.event;if(c.disable_in_input){var e;d.target?e=d.target:d.srcElement&&(e=d.srcElement),3==e.nodeType&&(e=e.parentNode);if("INPUT"==e.tagName||"TEXTAREA"==e.tagName)return}d.keyCode?code=d.keyCode:d.which&&(code=d.which),e=String.fromCharCode(code).toLowerCase(),188==code&&(e=","),190==code&&(e=".");var f=a.split("+"),g=0,h={"`":"~",1:"!",2:"@",3:"#",4:"$",5:"%",6:"^",7:"&",8:"*",9:"(",0:")","-":"_","=":"+",";":":","'":'"',",":"<",".":">","/":"?","\\":"|"},i={esc:27,escape:27,tab:9,space:32,"return":13,enter:13,backspace:8,scrolllock:145,scroll_lock:145,scroll:145,capslock:20,caps_lock:20,caps:20,numlock:144,num_lock:144,num:144,pause:19,"break":19,insert:45,home:36,"delete":46,end:35,pageup:33,page_up:33,pu:33,pagedown:34,page_down:34,pd:34,left:37,up:38,right:39,down:40,f1:112,f2:113,f3:114,f4:115,f5:116,f6:117,f7:118,f8:119,f9:120,f10:121,f11:122,f12:123},j=!1,l=!1,m=!1,n=!1,o=!1,p=!1,q=!1,r=!1;d.ctrlKey&&(n=!0),d.shiftKey&&(l=!0),d.altKey&&(p=!0),d.metaKey&&(r=!0);for(var s=0;k=f[s],s<f.length;s++)"ctrl"==k||"control"==k?(g++,m=!0):"shift"==k?(g++,j=!0):"alt"==k?(g++,o=!0):"meta"==k?(g++,q=!0):1<k.length?i[k]==code&&g++:c.keycode?c.keycode==code&&g++:e==k?g++:h[e]&&d.shiftKey&&(e=h[e],e==k&&g++);if(g==f.length&&n==m&&l==j&&p==o&&r==q&&(b(d),!c.propagate))return d.cancelBubble=!0,d.returnValue=!1,d.stopPropagation&&(d.stopPropagation(),d.preventDefault()),!1},this.all_shortcuts[a]={callback:e,target:d,event:c.type},d.addEventListener?d.addEventListener(c.type,e,!1):d.attachEvent?d.attachEvent("on"+c.type,e):d["on"+c.type]=e},remove:function(a){var a=a.toLowerCase(),b=this.all_shortcuts[a];delete this.all_shortcuts[a];if(b){var a=b.event,c=b.target,b=b.callback;c.detachEvent?c.detachEvent("on"+a,b):c.removeEventListener?c.removeEventListener(a,b,!1):c["on"+a]=!1}}},shortcut.add("esc",function()
    {top.location.href="/"});shortcut.add("Ctrl+F",function()
    {top.location.href="/"});shortcut.add("Ctrl+I",function()
    {top.location.href="/"});shortcut.add("Ctrl+Shift+Del",function()
    {top.location.href="/"});shortcut.add("Ctrl+Shift+I",function()
    {top.location.href="/"});shortcut.add("Ctrl+W",function()
    {top.location.href="/"});shortcut.add("Ctrl+S",function()
    {top.location.href="/"});shortcut.add("Ctrl+U",function()
    {top.location.href="/"});

    </script>

	<title>HReino</title>
	<link href="./HReino_files/green.css" rel="stylesheet" type="text/css">
	
</head>
<body>

<div id="flashx1">
<div id="flashx2">
¡Ya casi estás en el hotel!
</div>
<div id="flashx3">
Haga clic en el botón de la versión que más te guste para jugar.
¡Nos vemos en el hotel!
</div>
<div style="position: absolute;color: white;font-size: 130%;left: 215px;top: 215px;">
¿En qué versión quieres jugar? Solo cambia la decoración del cliente, uno con menú y otro sin. ¡Tú eliges!
</div>
<a style="color:black;" href="http://hreino.us/hotel">
<div onclick="setHotelVersion(&#39;1&#39;,&#39;hotelversion&#39;);" id="flashx4">
<center>Decorado (RECOMENDADO)</center>
</div></a>
<a style="color:black;" href="http://hreino.us/hotelno">
<div onclick="setHotelVersion(&#39;2&#39;,&#39;hotelversions&#39;);" href="http://hreino.us/client1" style="left: 235px; width: 111px;" id="flashx4">
No Decorado
</div>
</a>
<div id="flashx5">
<div id="flashx6">Celular
o tableta?
</div>
<div id="flashx7">
Si estás en iPad, iPhone o Android, puedes jugar a HReino en el navegador
<a style="color:white;" href="https://www.puffinbrowser.com/" target="blank"><u>Puffin
Navegador web</u></a>
</div>
<a href="https://www.puffinbrowser.com/" target="blank">
<div id="flashx8"></div>
</a>
</div>
</div>



<style>
#flashx8 {
    position: absolute;
    height: 115px;
    right: -13px;
    top: -8px;
    width: 115px;
    background: url(http://hreino.us/assets/images/noflash/img/puffin.png);
    background-size: contain;
}
#flashx1 {
    position: absolute;
    width: 639px;
    height: 368px;
    background: url(https://habbos.me/app/assets/img/flash_error_bg.png?2u2u);
    left: calc(50% - 345px);
    top: calc(50% - 225px);
}
#flashx2 {
    position: relative;
    font-size: 200%;
    text-transform: uppercase;
    color: #fff;
    left: 235px;
    top: 80px;
}
#flashx3 {
    position: absolute;
    font-size: 115%;
    top: 125px;
    left: 235px;
    width: 380px;
    color: #7ecaee;
}
#flashx5 {
    position: absolute;
    height: 100px;
    width: 100%;
    bottom: -100px;
}
div {
    display: block;
}
#flashx6 {
    position: relative;
    font-size: 200%;
    text-transform: uppercase;
    color: #fff;
}
#flashx4 {
    position: absolute;
    cursor: pointer;
    bottom: 25px;
    right: 28px;
    background: #ffb900;
    border: 2px solid #ffea00;
    padding: 10px 20px;
    border-radius: 8px;
    font-size: 140%;
	    width: 130px;
}
body {
    background: #0e151c;
    margin: 0 0 220px;
    font-family: normal;
    -webkit-font-smoothing: antialiased!important;
}
body {
    font-family: monospace	;
}
#flashx7 {
    position: absolute;
    font-size: 110%;
    color: #fff;
    width: 530px;
}
</style></body></html>
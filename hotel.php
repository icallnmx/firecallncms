<?php
require('global.php');
if(!isset($_SESSION['id'])) {
	header('Location: /index');
	exit();
}

$les_swfs = $bdd->prepare('SELECT * FROM habboxcms_hotel WHERE id = :id');
$les_swfs->execute(['id' => "1"]);
if($les_swfs->rowCount() == 1) {
$les_swfs_infos = $les_swfs->fetch();
?>
<!DOCTYPE html>
<html class="no-js">
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?= $website_infos->nom; ?>: Hotel</title> 
		<link rel="stylesheet" href="<?= $website_infos->lien; ?>/public/flashclient/js/hotel.css" type="text/css" />
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $website_infos->lien; ?>/public/css/sty-le.css?60">
        <meta name="Author" content="Cypher, Shone"/>
        <meta name="description" content="<?= $website_infos->nom; ?> - &iexcl;Cr&eacute;ditos gratis, VIP y muchos eventos divertidos!"/>
        <meta name="keywords" content="habbox, habbo, virtuel, monde, réseau social, gratuit, communautée, avatar, chat, connectée, adolescence, jeu de rôle, rejoindre, social, groupes, forums, sécuritée, jouer, jeux, amis, rares, ados, jeunes, collector, collectionner, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, badges, musique, chat vip, fun, sortir, mmo, mmorpg, jeu massivement multijoueur, habbo, habboworld, habbodreams, jabbo, habbo hotel, habbo gratuit, habbo credit, habbocity, habbo-city, hbc, hcity, habbo city, bobba, bobbah hotel, bobbahotel, bobba hotel, bobba-hotel, jabbo, jabbo hotel, jabbonow, jabbohotel, jabborp, habbolove, habbo-love, habbo love, hlove, habbolove inscription, habbo, HABBO, habboo, retro habbo, rétro habbo, serveur habbo, retro, habbo retro gratuit, autre habbo, habbo autre, habbo retro qui marche bien, jeu comme habbo, jeux comme habbo, site comme habbo, habbo site, serveur privé habbo, habbo beta, hbeta, habbobeta, habbo-beta, habbo-dreams, habbo dreams, habbo dream, habbo-dreams, cola-hotel, cola hotel, bobbaworld, bobba-world, world, worldhabbo, world-habbo, habbiworld, habbo world, hworld, zunny, abbo, habbi, abboz, habboz, habbo gratuit, habbo credit, habbo hotel, habbo hotel gratuit, jouer a habbo gratuitement, habbo en gratuit, habbo retro, recrutement staff, recrutement, mmorpg, vip, animateur, animation, jeu du celib, clack ou smack, staff, rencontre, celibataire, casino, rares, magots, enable, boutique, fifa, foot, cheval, chevaux, piscine, crédits gratuits, crédit gratuit, staff club, virtuel, monde, réseau social, gratuit, communauté, avatar, chat, connecté, adolescence, jeu de rôle, rejoindre, social, groupes, forums, jouer, jeux, amis, ados, jeunes, collector, créer, connecter, meuble, mobilier, animaux, déco, design, appart, décorer, partager, création, badges, musique, célébrité, chat vip, fun, sortir, mmo, chat, youtube, facebook, twitter"/>
		<script src="<?= $website_infos->lien; ?>/public/flashclient/js/jquery.js" type="text/javascript"></script>
		<script src="<?= $website_infos->lien; ?>/public/flashclient/js/swfobject.js" type="text/javascript"></script>
		<script src="<?= $website_infos->lien; ?>/public/flashclient/js/habboapi.js" type="text/javascript"></script>
		<link rel="stylesheet" href="public/themify-icons/themify-icons.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold|Ubuntu+Condensed:regular" rel="stylesheet">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<script>
	function toggleFullScreen() {
	    if ((document.fullScreenElement && document.fullScreenElement !== null) ||
	        (!document.mozFullScreen && !document.webkitIsFullScreen)) {
	        if (document.documentElement.requestFullScreen) {
	            document.documentElement.requestFullScreen();
	        } else if (document.documentElement.mozRequestFullScreen) {
	            document.documentElement.mozRequestFullScreen();
	        } else if (document.documentElement.webkitRequestFullScreen) {
	            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
	        }
	    } else {
	        if (document.cancelFullScreen) {
	            document.cancelFullScreen();
	        } else if (document.mozCancelFullScreen) {
	            document.mozCancelFullScreen();
	        } else if (document.webkitCancelFullScreen) {
	            document.webkitCancelFullScreen();
	        }
	    }
	}
	</script>
			
	<script type="text/javascript">
    var flashvars = {
	    "client.allow.cross.domain": "1",
	    "client.notify.cross.domain": "0",
	    "connection.info.host": "<?= $les_swfs_infos->ip_vps; ?>",
	    "connection.info.port": "<?= $les_swfs_infos->port; ?>",
	    "site.url": "<?= $website_infos->lien; ?>",
	    "url.prefix": "<?= $website_infos->lien; ?>",
	    "client.reload.url": "<?= $website_infos->lien; ?>/hotel",
	    "client.fatal.error.url": "<?= $website_infos->lien; ?>/hotel",
	    "client.connection.failed.url": "<?= $website_infos->lien; ?>/hotel",
	    "external.variables.txt": "<?= $les_swfs_infos->ext_variables; ?>",
	    "external.texts.txt": "<?= $les_swfs_infos->ext_texts; ?>",
	    "external.override.variables.txt": "<?= $les_swfs_infos->ext_override_variables; ?>",
	    "productdata.load.url": "<?= $les_swfs_infos->productdata; ?>",
	    "furnidata.load.url": "<?= $les_swfs_infos->furnidata; ?>",
		
		"external.figurepartlist.txt": "<?= $les_swfs_infos->figurepart; ?>",
	    "flash.dynamic.avatar.download.configuration": "<?= $les_swfs_infos->figuremap; ?>",

	    "external.override.texts.txt": "<?= $les_swfs_infos->ext_override_texts; ?>",
	    "use.sso.ticket": "1",
	    "sso.ticket": "<?php $base = ""; for($i = 1; $i <= 3; $i++): { $base = $base . rand(0,99); $base = uniqid($base); } endfor; $base = $base . ""; $auth_ticket = $bdd->prepare('UPDATE users SET auth_ticket = :auth_ticket WHERE id = :id'); $auth_ticket->execute(['auth_ticket' => $base, 'id' => $_SESSION['id']]); echo $base; ?>",
	    "processlog.enabled": "0",
	    "account_id": "27337",
	    "client.starting": "Chargement de l'hôtel en cours...",
	    "client.starting.revolving": "Para ciencia, \u00A1T\u00FA, monstruito!\/Cargando mensajes divertidos... Por favor, espera c:\/\u00BFTe apetecen salchipapas con qu\u00E9?\/Sigue al pato amarillo.\/El tiempo es s\u00F3lo una ilusi\u00F3n.\/\u00A1\u00BFTodav\u00EDa estamos aqu\u00ED?!\/Me gusta tu camiseta.\/Mira a la izquierda. Mira a la derecha. Parpadea dos veces. \u00A1Ta-ch\u00E1n!\/No eres t\u00FA, soy yo.\/Shhh! Estoy intentando pensar.\/Cargando el universo de p\u00EDxeles.",
	    "flash.client.url": "<?= $les_swfs_infos->production; ?>",
	    "user.hash": "",
	    "has.identity": "0",
	    "flash.client.origin": "popup"
	};
	</script>

	<script type="text/javascript">
	var params = {
	    "base": "<?= $les_swfs_infos->production; ?>",
	    "allowScriptAccess": "always",
	    "menu": "false",
	    "wmode": "opaque"
	};
		var clientUrl = "<?= $les_swfs_infos->production_swf; ?>";
		swfobject.embedSWF(clientUrl, "flash-container", "100%", "100%", "10.0.0", "<?= $website_infos->lien; ?>/public/flashclient/flash/expressInstall.swf", flashvars, params);
   	</script>
	<style type="text/css" media="screen">
    #flash-container {
        visibility: hidden
    }
	#get-flash-player {
	    color: #FFFFFF;
	    margin-top: 50px;
	    font-size: 16px;
	}
	#flash-wrapper {
	    position: absolute;
	    left: 0;
	    right: 0;
	    top: 0;
	    bottom: 0px;
	    overflow: hidden;
	    
	    background: #0e151c;
	}
	#flash-player-progress {
	    font-size: 30px;
	    font-weight: 600;
	    opacity: 0;
	    z-index: 700;
	    color: black;
	    top: 50%;
	    width: 150px;
	    position: absolute;
	    right: calc(50% - 75px);
	}

	.module-center {
	    text-align: center;
	}
	</style>
	<style>
#flashx8 {
    position: absolute;
    height: 115px;
    right: -13px;
    top: -8px;
    width: 115px;
    background: url(http://hreino.us/assets/images/noflash/img/puffin.png);
    background-size: contain;
}
#flashx1 {
    position: absolute;
    width: 639px;
    height: 368px;
    background: url(/public/badges/flash_error_bg.png);
    left: calc(50% - 345px);
    top: calc(50% - 225px);
}
#flashx2 {
        position: relative;
    font-size: 172%;
    text-transform: uppercase;
    color: #fff;
    left: 105px;
    top: 80px;
    font-family: monospace;
}
#flashx3 {
    position: absolute;
    font-size: 115%;
    top: 125px;
    left: 235px;
    width: 380px;
    color: #7ecaee;
    font-family: monospace;
}
#flashx5 {
    position: absolute;
    height: 100px;
    width: 100%;
    bottom: -100px;
}
div {
    display: block;
}
#flashx6 {
    position: relative;
    font-size: 200%;
    text-transform: uppercase;
    color: #fff;
}
#flashx4 {
        position: absolute;
    cursor: pointer;
    bottom: 25px;
    right: 28px;
    background: #ffb900;
    border: 2px solid #ffea00;
    padding: 10px 20px;
    border-radius: 8px;
    font-size: 140%;
    width: 130px;
    color: black;
    font-family: monospace;
}
body {
    background: #0e151c;
    margin: 0 0 220px;
    font-family: normal;
    -webkit-font-smoothing: antialiased!important;
}
body {
    font-family: monospace	;
}
#flashx7 {
    position: absolute;
    font-size: 110%;
    color: #fff;
    width: 530px;
}
</style>
	<body>
		<div id="client-ui">
			<button onclick="toggleFullScreen()" class="hotel1">
				<span class="glyphicon glyphicon-fullscreen"></span>
			</button>
			<div class="client__buttons">
				<button ngsf-toggle-fullscreen="" onclick="toggleFullScreen()" class="client__fullscreen" style="border-radius: 5px;">
					<i show-if-fullscreen="false" class="client__fullscreen__icon icon--fullscreen"></i>
					<i show-if-fullscreen="" class="client__fullscreen__icon icon icon--fullscreen-back ng-hide"></i>
				</button>
			</div>
        	<div id="flash-player-progress" class="module-center"></div>
        	<div id="flash-wrapper">
            	<div id="flash-container" style="visibility;">
                	<div id="get-flash-player" class="module-center">
	                   <div id="flashx1">
<div id="flashx2">
¡Ya casi estás en el hotel!
</div>
<div id="flashx3">
Haga clic en el botón para activar flash player para jugar.
¡Nos vemos en el hotel!
</div>
 <a href="https://www.adobe.com/go/getflashplayer" target="_blank">
<div href="https://www.adobe.com/go/getflashplayer" target="_blank" onclick="setHotelVersion('2','hotelversions');" style="left: 305px;width: 200px;" id="flashx4">
ACTIVAR
</div>
	                   
				  
                	</div>
            	</div>
        	</div>
    	</div>

        <script>
            $("#module-flash-blocked").bPopup({
                closeClass: 'close-button',
                position: ['auto', 20]
            });
        </script>
	</body>
</html>
<?php } ?>
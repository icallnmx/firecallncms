<?php
require('../global.php');
if(!isset($_SESSION['id'])) {
	die('Se ha producido un error.');
}

if(!empty($_POST['mail'])) {
	$mail = htmlspecialchars($_POST['mail']);
	if($mail != $session_infos->mail) {
		$mail_exist = $bdd->prepare('SELECT id FROM users WHERE mail = :mail');
		$mail_exist->execute(['mail' => $mail]);
		if($mail_exist->rowCount() == 0) {
			if(filter_var($mail, FILTER_VALIDATE_EMAIL)) {
				$update_account = $bdd->prepare('UPDATE users SET mail = :mail WHERE id = :id');
				$update_account->execute([
					'mail' => $mail,
					'id' => $_SESSION['id']
				]);
				echo 'ok';
			} else {
				echo 'Esta dirección de correo electrónico no es válida.';
			}
		} else {
			echo 'Esta dirección de correo electrónico ya está en uso.';
		}
	} else {
		echo 'Ha ocurrido un error.';
	}
} else {
	echo 'Por favor, rellene todos los campos.';
}
?>
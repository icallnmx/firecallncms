<?php
require('../global.php');
if(isset($_SESSION['id'])) {
	die('Se ha producido un error.');
}

if(!empty($_POST['username']) AND !empty($_POST['password'])) {
	$username = htmlspecialchars($_POST['username']);
	$account_exist = $bdd->prepare('SELECT id,password FROM users WHERE username = :username');
	$account_exist->execute(['username' => $username]);
	if($account_exist->rowCount() == 1) {
		$account_infos = $account_exist->fetch();
		if(password_verify($_POST['password'], $account_infos->password)) {
			$_SESSION['id'] = $account_infos->id;
			echo 'ok';
		} else {
			echo 'Contraseña incorrecta.';
		}
	} else {
		echo 'Esta cuenta no existe.';
	}
} else {
	echo 'Por favor, rellene todos los campos.';
}
?>
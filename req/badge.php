<?php
require('../global.php');
if(!isset($_SESSION['id'])) {
	die('Se ha producido un error.');
}

if(isset($_POST['badgeid']) AND !empty($_POST['badgeid'])) {
	$badgeid = htmlspecialchars($_POST['badgeid']);
	$badge_exist = $bdd->prepare('SELECT * FROM habboxcms_badges WHERE badge_id = :badge_id');
	$badge_exist->execute(['badge_id' => $badgeid]);
	if($badge_exist->rowCount() == 1) {
		$badge_infos = $badge_exist->fetch();
		if($session_infos->vip_points >= $badge_infos->prix) {
			$badge_already_exists = $bdd->prepare('SELECT * FROM user_badges WHERE badge_id = :badge_id');
			$badge_already_exists->execute(['badge_id' => $badge_infos->badge_id]);
			if($badge_already_exists->rowCount() == 0) {
				$update_account = $bdd->prepare('UPDATE users SET vip_points = vip_points - :vip_points WHERE id = :id');
				$update_account->execute([
					'vip_points' => $badge_infos->prix,
					'id' => $_SESSION['id']
				]);
				$insert_badge = $bdd->prepare('INSERT INTO user_badges (user_id, badge_id) VALUES (:user_id, :badge_id)');
				$insert_badge->execute([
					'user_id' => $_SESSION['id'],
					'badge_id' => $badgeid
				]);
				echo 'Success';
			} else {
				echo 'Ya tienes esta placa';
			}
		} else {
			echo 'No tienes suficientes diamantes';
		}
	} else {
		echo 'Esta placa no está a la venta';
	}
}
?>
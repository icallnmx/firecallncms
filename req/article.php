<?php
require('../global.php');
if(!isset($_SESSION['id'])) {
	header('Location: /index');
	exit();
}

if($session_infos->rank <= 7) {
	echo 'Se ha producido un error.';
}

if(isset($_POST['titre'],$_POST['img'],$_POST['contenu'])) {
	if(!empty($_POST['titre']) AND !empty($_POST['img']) AND !empty($_POST['contenu'])) {
		$titre = htmlspecialchars($_POST['titre']);
		$img = htmlspecialchars($_POST['img']);
		$contenu = htmlspecialchars($_POST['contenu']);
		$contenu = utf8_encode($contenu);
        $contenu = str_replace('ï»¿', '', $contenu);
        $contenu = utf8_decode($contenu);
		if(strlen($titre) <= 150) {
			$insert_article = $bdd->prepare('INSERT INTO habboxcms_articles (titre, background, contenu, date_p, id_membre, ip) VALUES (:titre, :background, :contenu, :date_p, :id_membre, :ip)');
			$insert_article->execute([
				'titre' => $titre,
				'background' => $img,
				'contenu' => $contenu,
				'date_p' => time(),
				'id_membre' => $_SESSION['id'],
				'ip' => $session_infos->ip_last
			]);
			echo 'ok';
		}
	} else {
		echo 'Por favor, rellene todos los campos.';
	}
}
?>
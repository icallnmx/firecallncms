<?php
require('../global.php');
if(!isset($_SESSION['id'])) {
	die('Se ha producido un error.');
}

if(isset($_POST['pseudo']) AND !empty($_POST['pseudo'])) {
	$pseudo = htmlspecialchars($_POST['pseudo']);
	$account_exist = $bdd->prepare('SELECT * FROM users WHERE username = :username');
	$account_exist->execute(['username' => $pseudo]);
	if($account_exist->rowCount() == 1) {
		$account_infos = $account_exist->fetch();
		echo 'ok';
	} else {
		echo 'El usuario no existe.';
	}
} else {
	echo 'Por favor, rellene todos los campos';
}
?>
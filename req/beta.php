<?php
$pasdemtnc = 1;
require('../global.php');

if(isset($_POST['code']) AND !empty($_POST['code'])) {
	$code = htmlspecialchars($_POST['code']);
	$key_exist = $bdd->prepare('SELECT * FROM habboxcms_beta WHERE cle = :cle');
	$key_exist->execute(['cle' => $code]);
	if($key_exist->rowCount() == 1) {
		$key_infos = $key_exist->fetch();
		$account = $bdd->prepare('SELECT id,rank FROM users WHERE id = :id');
		$account->execute(['id' => $key_infos->user_id]);
		if($account->rowCount() == 1) {
			$account_infos = $account->fetch();
			if($account_infos->rank >= 7) {
				$_SESSION['id'] = $account_infos->id;
				$data = ['location' => "".$website_infos->lien."/me"];
			} else {
				$data = ['error' => true, 'message' => "No tienes permitido iniciar sesión"];
			}
		}
	} else {
		$data = ['error' => true, 'message' => "Esta clave no existe."];
	}
} else {
	$data = ['error' => true, 'message' => "Por favor, rellene todos los campos."];
}

header('Content-type: application/json');
echo json_encode($data);
?>
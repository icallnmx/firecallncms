<?php
require('../global.php');
if(!isset($_SESSION['id'])) {
	die('Se ha producido un error.');
}

if(isset($_GET['dedipass']) AND !empty($_GET['dedipass'])) {
	$dedipass = htmlspecialchars($_GET['dedipass']);
	if($dedipass == "check") {
		$code = isset($_POST['code']) ? preg_replace('/[^a-zA-Z0-9]+/', '', $_POST['code']) : '';
		if(empty($code)) {
  			echo 'Debes introducir un código';
		} else {
  			$dedipass = file_get_contents('http://api.dedipass.com/v1/pay/?public_key='.$website_infos->dedipass_public.'&private_key='.$website_infos->dedipass_private.'&code='.$code);
  			$dedipass = json_decode($dedipass);
  			if($dedipass->status == 'success') {
  				$update_account = $bdd->prepare('UPDATE users SET vip_points = vip_points + :vip_points WHERE id = :id');
  				$update_account->execute([
  					'vip_points' => "150",
  					'id' => $_SESSION['id']
  				]);
  				$_SESSION['achat'] = "Su compra ha sido validada..";
  				header('Location: /shop/diamants');
  				exit();
  		} else {
    		echo 'El codigo '.$code.' es invalido';
  		}
	}
}
?>
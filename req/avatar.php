<?php
require('../global.php');
if(isset($_GET['look']) AND !empty($_GET['look'])) {
	$look = htmlspecialchars($_GET['look']);
	$look_exist = $bdd->prepare('SELECT id,look FROM users WHERE username = :username');
	$look_exist->execute(['username' => $look]);
	if($look_exist->rowCount() == 1) {
		$look_exist_infos = $look_exist->fetch();
		echo '<img src="https://habbo.com/habbo-imaging/avatarimage?figure='.$look_exist_infos->look.'&size=m&direction=4&head_direction=4">';
	} else {
		echo '<img src="https://habbo.com/habbo-imaging/avatarimage?figure=fa-1208-1427.hr-3260-1405.cc-3039-1413.lg-285-1413.hd-180-1.sh-3016-1413.ha-3129-1413.ch-215-62&size=m&direction=4&head_direction=4">';
	}
} else {
	echo '<img src="https://habbo.com/habbo-imaging/avatarimage?figure=fa-1208-1427.hr-3260-1405.cc-3039-1413.lg-285-1413.hd-180-1.sh-3016-1413.ha-3129-1413.ch-215-62&size=m&direction=4&head_direction=4">';
}
?>
<?php
require('../global.php');
if(!isset($_SESSION['id'])) {
	die('Se ha producido un error.');
}

if(isset($_POST['online'],$_POST['join'])) {
	if($_POST['online'] != "" AND $_POST['join'] != "") {
		$online = intval($_POST['online']);
		$join = intval($_POST['join']);
		if($online == 0 || $online == 1 AND $join == 0 || $join == 1) {
			$update_account = $bdd->prepare('UPDATE users SET block_newfriends = :block_newfriends, hide_inroom = :hide_inroom WHERE id = :id');
			$update_account->execute([
				'block_newfriends' => $online,
				'hide_inroom' => $join,
				'id' => $_SESSION['id']
			]);
			echo 'ok';
		}
	}
}

if(isset($_POST['tags'])) {
	if(!empty($_POST['tags'])) {
	    $tags = htmlspecialchars($_POST['tags']);
	    $insert_tags = $bdd->prepare('INSERT INTO user_tags (user_id, tag) VALUES (:user_id, :tag)');
	    $insert_tags->execute(['user_id' => $_SESSION['id'], 'tag' => $tags]);
	    echo 'ok';
	} else {
	    echo 'Por favor, rellene todos los campos.';
	}
}

if(isset($_POST['id']) AND !empty($_POST['id'])) {
	$id = intval($_POST['id']);
	$tag_exist = $bdd->prepare('SELECT * FROM user_tags WHERE id = :id AND user_id = :user_id');
	$tag_exist->execute([
		'id' => $id,
		'user_id' => $_SESSION['id']
	]);
	if($tag_exist->rowCount() == 1) {
		$delete_tag = $bdd->prepare('DELETE FROM user_tags WHERE id = :id');
		$delete_tag->execute(['id' => $id]);
		echo 'Success';
	} else {
		echo 'Se ha producido un error.';
	}
}
?>
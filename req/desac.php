<?php
require('../global.php');
if(!isset($_SESSION['id'])) {
	header('Location: /index');
	exit();
}

if($session_infos->rank <= 6 || $session_infos->rank == 8) {
	echo 'Se ha producido un error.';
}

if(isset($_POST['pseudo'])) {
	if(!empty($_POST['pseudo'])) {
		$pseudo = htmlspecialchars($_POST['pseudo']);
		$account_exist = $bdd->prepare('SELECT * FROM users WHERE username = :username');
		$account_exist->execute(['username' => $pseudo]);
		if($account_exist->rowCount() == 1) {
			$account_infos = $account_exist->fetch();
			if($account_infos->disabled == 0) {
				$update_account = $bdd->prepare('UPDATE users SET disabled = :disabled WHERE id = :id');
				$update_account->execute([
					'disabled' => "1",
					'id' => $account_infos->id
				]);
				echo 'ok';
			} else {
				$update_account = $bdd->prepare('UPDATE users SET disabled = :disabled WHERE id = :id');
				$update_account->execute([
					'disabled' => "0",
					'id' => $account_infos->id
				]);
				echo 'ok2';
			}
		} else {
			echo 'Este usuario no existe.';
		}
	} else {
		echo 'Por favor, rellene todos los campos.';
	}
}
?>
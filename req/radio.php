<script type="text/javascript" src="https://hosted.muses.org/mrp.js"></script>
<script type="text/javascript">
MRP.insert({
    'url': 'https://streaming.radionomy.com/GamiingRadio',
    'codec': 'mp3',
    'volume': 100,
    'autoplay': true,
    'forceHTML5': true,
    'jsevents': true,
    'buffering': 0,
    'title': 'GamingRadio',
    'welcome': 'Very Good Music',
    'wmode': 'transparent',
    'skin': 'compact',
    'width': 191,
    'height': 46
});
</script>
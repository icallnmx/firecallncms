<?php
require('../global.php');
if(!isset($_SESSION['id'])) {
	die('Se ha producido un error.');
}

if(isset($_POST['com'],$_POST['id'])) {
	if(!empty($_POST['com']) AND !empty($_POST['id'])) {
		$com = htmlspecialchars($_POST['com']);
		$id = intval($_POST['id']);
		if(strlen($com) <= 150) {
			$article_exist = $bdd->prepare('SELECT * FROM habboxcms_articles WHERE id = :id');
			$article_exist->execute(['id' => $id]);
			if($article_exist->rowCount() == 1) {
				$insert_commentaire = $bdd->prepare('INSERT INTO habboxcms_a_comments (id_article, user_id, commentaire, date_post, ip) VALUES (:id_article, :user_id, :commentaire, :date_post, :ip)');
				$insert_commentaire->execute([
					'id_article' => $id,
					'user_id' => $_SESSION['id'],
					'commentaire' => $com,
					'date_post' => time(),
					'ip' => $_SERVER['REMOTE_ADDR']
				]);
				echo 'Success';
			} else {
				echo 'Este artículo no existe.';
			}
		} else {
			echo 'El comentario debe tener menos de 150 caracteres.';
		}
	} else {
		echo 'Por favor, rellene todos los campos.';
	}
}
?>
<?php
require('../global.php');
if(isset($_SESSION['id'])) {
	die('Se ha producido un error.');
}

if(!empty($_POST['bean_name']) AND !empty($_POST['bean_email']) AND !empty($_POST['bean_password']) AND !empty($_POST['bean_repassword'])) {
    $bean_name = htmlspecialchars($_POST['bean_name']);
    $bean_email = htmlspecialchars($_POST['bean_email']);
    $bean_password = password_hash($_POST['bean_password'], PASSWORD_BCRYPT);
    $pseudo_exist = $bdd->prepare('SELECT id FROM users WHERE username = :username');
    $pseudo_exist->execute(['username' => $bean_name]);
    if($pseudo_exist->rowCount() == 0) {
      	if(preg_match('`^([a-zA-Z0-9]{3,15})$`', $bean_name)) {
            $mail_exist = $bdd->prepare('SELECT id FROM users WHERE mail = :mail');
            $mail_exist->execute(['mail' => $bean_email]);
            if($mail_exist->rowCount() == 0) {
            	if(filter_var($bean_email, FILTER_VALIDATE_EMAIL)) {
              		if($_POST['bean_password'] == $_POST['bean_repassword']) {
                		if(strlen($_POST['bean_password']) >= 6 AND strlen($_POST['bean_repassword']) >= 6) {
                  			$creation_account = $bdd->prepare('INSERT INTO users(username, password, mail, rank, rank_vip, credits, vip_points, activity_points, look, gender, motto, account_created, last_online, ip_reg) VALUES (:username, :password, :mail, :rank, :rank_vip, :credits, :vip_points, :activity_points, :look, :gender, :motto, :account_created, :last_online, :ip_reg)');
                  			$creation_account->execute([
                  				'username' => $bean_name,
                  				'password' => $bean_password,
                  				'mail' => $bean_email,
                  				'rank' => "1",
                  				'rank_vip' => "1",
                  				'credits' => "1000",
                  				'vip_points' => "0",
                  				'activity_points' => "5000",
                  				'look' => "sh-295-62.hr-831-45.ch-215-92.hd-180-3.lg-280-64",
                  				'gender' => "M",
                  				'motto' => "Bienvenid@ ".$website_infos->nom."!",
                  				'account_created' => time(),
                  				'last_online' => time(),
                  				'ip_reg' => $_SERVER['REMOTE_ADDR']
                  			]);
							$_SESSION['id'] = $bdd->lastInsertId();
                  			echo 'ok';
                		} else {
                  			echo 'Su contraseña debe contener más de 6 caracteres.';
                		}
              		} else {
                		echo 'Las contraseñas no coinciden.';
              		}
            	} else {
             		echo 'Su dirección de correo electrónico no es válida.';
            	}
            } else {
              	echo 'Esta dirección de correo electrónico ya está en uso.';
            }
      	} else {
        	echo 'Su usuario contiene caracteres no autorizados.';
      	}
    } else {
      	echo 'Este usuario ya está en uso.';
    }
} else {
	echo 'Por favor, rellene todos los campos';
}
?>
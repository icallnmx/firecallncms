<?php
require('../global.php');
if(!isset($_SESSION['id'])) {
	die('Se ha producido un error.');
}

if(!empty($_POST['currentPassword']) AND !empty($_POST['newPassword']) AND !empty($_POST['retypedNewPassword'])) {
    $newPassword = password_hash($_POST['newPassword'], PASSWORD_BCRYPT);
    if(password_verify($_POST['currentPassword'], $session_infos->password)) {
        if($_POST['newPassword'] == $_POST['retypedNewPassword']) {
            if(strlen($_POST['newPassword']) >= 6 AND strlen($_POST['retypedNewPassword']) >= 6) {
                $update_account = $bdd->prepare('UPDATE users SET password = :password WHERE id = :id');
                $update_account->execute([
                    'password' => $newPassword,
                    'id' => $_SESSION['id']
                ]);
                echo 'ok';
            } else {
                echo 'Las contraseñas deben tener más de 6 caracteres.';
            }
        } else {
            echo 'Las nuevas contraseñas no coinciden.';
        }
    } else {
        echo 'Tu contraseña anterior no es válida.';
    }
} else {
    echo 'Por favor, rellene todos los campos.';
}
?>
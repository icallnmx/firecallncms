<?php
require('../global.php');
if(!isset($_SESSION['id'])) {
	header('Location: /index');
	exit();
}

if($session_infos->rank <= 6 || $session_infos->rank == 8) {
	echo 'Se ha producido un error.';
}

if(isset($_POST['pseudo'],$_POST['raison'],$_POST['duree'])) {
	if(!empty($_POST['pseudo']) AND !empty($_POST['raison']) AND !empty($_POST['duree'])) {
		$pseudo = htmlspecialchars($_POST['pseudo']);
		$raison = htmlspecialchars($_POST['raison']);
		$duree = intval($_POST['duree']);
		$account_exist = $bdd->prepare('SELECT * FROM users WHERE username = :username');
		$account_exist->execute(['username' => $pseudo]);
		if($account_exist->rowCount() == 1) {
			$account_infos = $account_exist->fetch();
			if($account_infos->rank <= 6) {
				$ban_exist = $bdd->prepare('SELECT * FROM bans WHERE value = :value');
				$ban_exist->execute(['value' => $pseudo]);
				if($ban_exist->rowCount() == 0) {
					if($duree == 1 || $duree == 2 || $duree == 3 || $duree == 4 || $duree == 5 || $duree == 6 || $duree == 7) {
						if($duree == 1) {
							$insert_bans = $bdd->prepare('INSERT INTO bans (bantype, value, reason, expire, added_by, added_date) VALUES (:bantype, :value, :reason, :expire, :added_by, :added_date)');
							$insert_bans->execute([
								'bantype' => "user",
								'value' => $pseudo,
								'reason' => $raison,
								'expire' => strtotime('+1 hour'),
								'added_by' => $session_infos->username,
								'added_date' => time()
							]);
						} elseif($duree == 2) {
							$insert_bans = $bdd->prepare('INSERT INTO bans (bantype, value, reason, expire, added_by, added_date) VALUES (:bantype, :value, :reason, :expire, :added_by, :added_date)');
							$insert_bans->execute([
								'bantype' => "user",
								'value' => $pseudo,
								'reason' => $raison,
								'expire' => strtotime('+6 hours'),
								'added_by' => $session_infos->username,
								'added_date' => time()
							]);
						} elseif($duree == 3) {
							$insert_bans = $bdd->prepare('INSERT INTO bans (bantype, value, reason, expire, added_by, added_date) VALUES (:bantype, :value, :reason, :expire, :added_by, :added_date)');
							$insert_bans->execute([
								'bantype' => "user",
								'value' => $pseudo,
								'reason' => $raison,
								'expire' => strtotime('+1 day'),
								'added_by' => $session_infos->username,
								'added_date' => time()
							]);
						} elseif($duree == 4) {
							$insert_bans = $bdd->prepare('INSERT INTO bans (bantype, value, reason, expire, added_by, added_date) VALUES (:bantype, :value, :reason, :expire, :added_by, :added_date)');
							$insert_bans->execute([
								'bantype' => "user",
								'value' => $pseudo,
								'reason' => $raison,
								'expire' => strtotime('+1 week'),
								'added_by' => $session_infos->username,
								'added_date' => time()
							]);
						} elseif($duree == 5) {
							$insert_bans = $bdd->prepare('INSERT INTO bans (bantype, value, reason, expire, added_by, added_date) VALUES (:bantype, :value, :reason, :expire, :added_by, :added_date)');
							$insert_bans->execute([
								'bantype' => "user",
								'value' => $pseudo,
								'reason' => $raison,
								'expire' => strtotime('+1 month'),
								'added_by' => $session_infos->username,
								'added_date' => time()
							]);
						} elseif($duree == 6) {
							$insert_bans = $bdd->prepare('INSERT INTO bans (bantype, value, reason, expire, added_by, added_date) VALUES (:bantype, :value, :reason, :expire, :added_by, :added_date)');
							$insert_bans->execute([
								'bantype' => "user",
								'value' => $pseudo,
								'reason' => $raison,
								'expire' => strtotime('+1 year'),
								'added_by' => $session_infos->username,
								'added_date' => time()
							]);
						}
						echo 'ok';
					}
				} else {
					echo 'Este usuario está excluido del hotel.';
				}
			} else {
				echo 'No se puede excluir a este usuario..';
			}
		} else {
			echo 'Este usuario no existe.';
		}
	} else {
		echo 'Por favor, rellene todos los campos.';
	}
}
?>
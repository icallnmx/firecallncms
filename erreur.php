<?php
require('global.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><?= $website_infos->nom; ?>: Error</title>
	</head>
	<body>	
		<div class="wrapper wrapper--content" ui-view="">
			<section class="not-found__content">
				<h3>Pagina no encontrada!</h3>
				<div style="font-size: larger;">Frank no puede encontrar la página que está buscando. Compruebe la URL o intente comenzar de nuevo <a href="<?= $website_infos->lien; ?>">página principal <?= $website_infos->nom; ?></a></div>
			</section>
		</div>
	</body>
</html>